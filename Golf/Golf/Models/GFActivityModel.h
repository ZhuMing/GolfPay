//
//  GFActivityModel.h
//  Golf
//
//  Created by 朱明 on 15/1/29.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GFActivityModel : NSObject

@property (nonatomic,strong) NSString *IndividualityTagName;
@property (nonatomic,strong) NSString *ActivityName;
@property (nonatomic,strong) NSString *ActivityPhoto;
@property (nonatomic,strong) NSString *AreaTagName;
@property (nonatomic,strong) NSString *Id;
@property (nonatomic,strong) NSString *ITagName;
@property (nonatomic,strong) NSString *ATagName;
@property (nonatomic,strong) NSString *BTagName;

@property (nonatomic,strong) NSMutableArray *tagList;


-(instancetype)initWithDic:(NSDictionary *)dic;
@end
