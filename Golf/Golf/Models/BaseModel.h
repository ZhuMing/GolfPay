//
//  BaseModel.h
//  Golf
//
//  Created by 朱明 on 14-5-15.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>

/* getter setter */
#ifndef SYNTHESIZE_STRONG
#define SYNTHESIZE_STRONG(varType, varName)\
@property (nonatomic, strong) varType varName;
#endif

/* getter */
#ifndef SYNTHESIZE_READONLY
#define SYNTHESIZE_READONLY(varType, varName)\
@property (nonatomic, readonly) varType varName;
#endif

#ifndef Model
#define Model(varType,dic)\
[varType initWithDic:dic]
#endif

#import "JSONModel.h"
@interface BaseModel : JSONModel<NSCopying>

@property (nonatomic, assign) NSInteger      status;
@property (nonatomic, strong) NSString       *errmsg;
@property (nonatomic, assign) BOOL           success;

/* 初始化 Virtual */
- (id)initWithDic:(NSDictionary *)dic;//=0

/* 打印对象 */
- (void)print;

@end
