//
//  GFUserInfoModel.h
//  Golf
//
//  Created by 朱明 on 15/3/8.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "BaseModel.h"

@interface GFUserInfoModel : BaseModel

@property (nonatomic,strong) NSString *Cover;
@property (nonatomic,strong) NSString *Id;
@property (nonatomic,assign) BOOL State;
@property (nonatomic,strong) NSString *Sex;
@property (nonatomic,strong) NSString *Account;
@property (nonatomic,strong) NSString *Password;
@property (nonatomic,strong) NSString *Logo;
@property (nonatomic,strong) NSString *CreateDate;
@property (nonatomic,strong) NSString *NikeName;
@property (nonatomic,assign) NSInteger Age;




/* 同步到本地 */
- (void)synchronousLocal;

- (id)initWithLocal;
@end
