//
//  FPUploadIMG.m
//  FindPeople
//
//  Created by 朱明 on 14/11/29.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import "FPUploadIMG.h"

@implementation FPUploadIMG
-(instancetype)initWithImg:(UIImage*)img
                   andIsUd:(BOOL)isupload
{
    if (self = [super init]) {
        self.img = img;
        self.isUpLoad = isupload;
    }
    return self;
}

@end
