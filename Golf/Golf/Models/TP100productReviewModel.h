//
//  TP100productReviewModel.h
//  Golf
//
//  Created by zhouhong on 3/7/15.
//  Copyright (c) 2015 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TP100productReviewModel : NSObject

@property (nonatomic,strong) NSString *ReviewId;
@property (nonatomic,strong) NSString *ProductId;
@property (nonatomic,strong) NSString *CustomerId;
@property (nonatomic,strong) NSString *Review;

-(instancetype)initWithDic:(NSDictionary*)dic;


@end
