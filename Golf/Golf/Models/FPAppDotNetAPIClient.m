

#import "FPAppDotNetAPIClient.h"



@implementation FPAppDotNetAPIClient

+ (instancetype)sharedClient {
    static FPAppDotNetAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[FPAppDotNetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:FPAppDotNetAPIBaseURLString]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    
    return _sharedClient;
}

@end
