//
//  BaseModel.m
//  Golf
//
//  Created by 朱明 on 14-5-15.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import "BaseModel.h"
#import <objc/runtime.h>

@implementation BaseModel

- (id)initWithDic:(NSDictionary *)dic
{
    self=[super initWithDictionary:dic error:nil];
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    BaseModel *model = [[self class] allocWithZone:zone];
    return model;
}


- (void)print
{
    NSLog(@"%@:%@",[self class],[self properties_aps]);
}

- (NSDictionary *)properties_aps
{
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    for (i = 0; i<outCount; i++)
    {
        objc_property_t property = properties[i];
        const char* char_f =property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:char_f];
        id propertyValue = [self valueForKey:(NSString *)propertyName];
        if (propertyValue) [props setObject:propertyValue forKey:propertyName];
    }
    free(properties);
    return props;
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
