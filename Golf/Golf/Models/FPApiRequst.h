//
//  FPGetApiRequst.h
//  FindPeople
//
//  Created by 朱明 on 14/11/25.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPAPPClient.h"

@interface FPApiRequst : NSObject

+ (void)LoginRequstWithUrl:(NSString*)url
                      andView:(id)vc
                       andMSG:(NSString*)msg
                andParameters:(NSMutableDictionary*)parameters
                        Block:(void (^)(id objc, NSError *error))block;




+ (void)getRequstWithUrl:(NSString*)url
                                   andView:(id)vc
                                    andMSG:(NSString*)msg
                             andParameters:(NSMutableDictionary*)parameters
                                     Block:(void (^)(id objc, NSError *error))block;

+ (void)PostRequstWithUrl:(NSString*)url
                                   andView:(id)vc
                                    andMSG:(NSString*)msg
                             andParameters:(NSMutableDictionary*)parameters
                                     Block:(void (^)(id objc, NSError *error))block;

+ (void) PostRequstWithUrl:(NSString*)url
                   andView:(id)vc
                    andMSG:(NSString*)msg
                andIMGName:(NSString*)fileName
andIsRealNameCertification:(BOOL)isRNC
             andParameters:(NSMutableDictionary*)parameters
                 andImages:(NSMutableArray*)imgs
                     Block:(void (^)(id objc, NSError *error))block;

+ (AFHTTPRequestOperation *)postPostAddressWithUrl:(NSString *)url
                                     andAttributes:(NSMutableArray *)attributes
                                          andBlock:(void (^)(id response, NSError *error))block;
@end
