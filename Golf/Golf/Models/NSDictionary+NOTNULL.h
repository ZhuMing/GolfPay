//
//  NSDictionary+NOTNULL.h
//  FindPeople
//
//  Created by 朱明 on 14/11/26.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NOTNULL)

-(NSArray*)ArrayForKey:(id)aKey;

-(NSDictionary*)DictionaryForKey:(id)aKey;

-(NSString*)StringForKey:(id)aKey;

-(id)ObjectForKey:(id)aKey;

@end
