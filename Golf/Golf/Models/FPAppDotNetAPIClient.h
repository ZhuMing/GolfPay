

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

@interface FPAppDotNetAPIClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end
