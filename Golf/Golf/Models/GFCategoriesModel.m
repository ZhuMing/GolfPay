//
//  GFCategoriesModel.m
//  Golf
//
//  Created by 朱明 on 15/1/29.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFCategoriesModel.h"

@implementation GFCategoriesModel
-(instancetype)initWithDic:(NSDictionary*)dic
{
    if (self = [super init]) {
        self.Id = dic[@"Id"];
        self.TypeName = dic[@"TypeName"];
    }
    return self;
}
@end
