//
//  FPUploadIMG.h
//  FindPeople
//
//  Created by 朱明 on 14/11/29.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FPUploadIMG : NSObject

@property (nonatomic,strong) UIImage *img;
@property (nonatomic,assign) BOOL isUpLoad;


-(instancetype)initWithImg:(UIImage*)img
                   andIsUd:(BOOL)isupload;

@end
