//
//  GFAPPClient.h
//  GolfFun
//
//  Created by 朱明 on 14-5-15.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface FPAPPClient : AFHTTPRequestOperationManager

+ (instancetype)sharedClient;

@end
