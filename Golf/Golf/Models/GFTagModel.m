//
//  GFTagModel.m
//  Golf
//
//  Created by 朱明 on 15/2/8.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFTagModel.h"

@implementation GFTagModel
-(instancetype)initWithDic:(NSDictionary*)dic
{
    if (self = [super init]) {
        self.TagName = dic[@"TagName"];
        self.Id = dic[@"Id"];
        self.CountryName= dic[@"CountryName"];
    }
    return self;
}
@end
