//
//  TP100productReviewModel.m
//  Golf
//
//  Created by zhouhong on 3/7/15.
//  Copyright (c) 2015 ZHU. All rights reserved.
//

#import "TP100productReviewModel.h"

@implementation TP100productReviewModel


-(instancetype)initWithDic:(NSDictionary*)dic
{
    if (self = [super init]) {
        self.ReviewId = dic[@"Id"];
        self.ProductId = dic[@"ProductId"];
        self.CustomerId = dic[@"CustomerId"];
        self.Review = dic[@"Review"];
    }
    return self;
}

@end
