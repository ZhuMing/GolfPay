//
//  GFTagModel.h
//  Golf
//
//  Created by 朱明 on 15/2/8.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GFTagModel : NSObject
@property (nonatomic,strong) NSString *TagName;
@property (nonatomic,strong) NSString *Id;
@property (nonatomic,strong) NSString *CountryName;

-(instancetype)initWithDic:(NSDictionary*)dic;




@end
