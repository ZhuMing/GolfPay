//
//  TP100ProductModel.m
//  Golf
//
//  Created by zhouhong on 3/7/15.
//  Copyright (c) 2015 ZHU. All rights reserved.
//

#import "TP100ProductModel.h"

@implementation TP100ProductModel


-(instancetype)initWithDic:(NSDictionary*)dic
{
    if (self = [super init]) {
        self.proId = dic[@"Id"];
        self.ProductName = dic[@"ProductName"];
        self.Price = [dic[@"Price"]integerValue] ;
        self.Stock = [dic[@"Stock"]integerValue];
        self.Score = [dic[@"Score"]integerValue];
        self.Sales = [dic[@"Sales"]integerValue];
        self.Type = dic[@"Type"];
        self.CreateDate = dic[@"CreateDate"];
        self.ActivityId = dic[@"ActivityId"];
        self.ProductContent = dic[@"ProductContent"];
        self.ATagName = [dic StringForKey:@"ATagName"];
        self.BTagName = [dic StringForKey:@"BTagName"];
        self.ITagName = [dic StringForKey:@"ITagName"];
        
        self.tagList = [NSMutableArray arrayWithCapacity:3];
        
        if (self.ATagName.length>0) {
            [self.tagList addObject:self.ATagName];
        }
        if (self.BTagName.length>0) {
            [self.tagList addObject:self.BTagName];
        }
        if (self.ITagName.length>0) {
            [self.tagList addObject:self.ITagName];
        }
        
        
    }
    return self;
}


@end
