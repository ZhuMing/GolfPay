//
//  FPGetApiRequst.m
//  FindPeople
//
//  Created by 朱明 on 14/11/25.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import "FPApiRequst.h"
#import "FPUploadIMG.h"

@implementation FPApiRequst


+ (void)LoginRequstWithUrl:(NSString*)url
                 andView:(id)vc
                  andMSG:(NSString*)msg
           andParameters:(NSMutableDictionary*)parameters
                   Block:(void (^)(id objc, NSError *error))block
{
    UIViewController *ViewController = (UIViewController*)vc;
    [vc showHudInView:ViewController.view hint:msg];
    AFHTTPRequestOperation *operation = [[FPAPPClient sharedClient]POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [ViewController hideHud];
        if ([responseObject[@"code"]integerValue]==401||[responseObject isKindOfClass:[NSNull class]])
        {
            
            return ;
        }
        else if([responseObject[@"code"]integerValue]!=0&&[responseObject[@"code"]integerValue]!=200)
        {
            
            return ;
        }
        NSString *TokenString = [[operation.response allHeaderFields] valueForKey:@"HEADER_SECURITY_TOKEN"];
        [[NSUserDefaults standardUserDefaults]setObject:TokenString forKey:@"HEADER_SECURITY_TOKEN"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        if (block) {
            block(responseObject, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ViewController hideHud];
        if (block) {
            block(operation, nil);
        }
    }];
    [UIAlertView showAlertViewForRequestOperationWithErrorOnCompletion:operation delegate:nil];
}

+ (void)getRequstWithUrl:(NSString*)url
                 andView:(id)vc
                  andMSG:(NSString*)msg
           andParameters:(NSMutableDictionary*)parameters
                   Block:(void (^)(id objc, NSError *error))block
{
    UIViewController *ViewController = nil;
    if (vc!=nil) {
        ViewController = (UIViewController*)vc;
        [vc showHudInView:ViewController.view hint:msg];
    }
    NSURLSessionDataTask *task = [[FPAppDotNetAPIClient sharedClient] GET:url parameters:parameters success:^(NSURLSessionDataTask * __unused task, id JSON)
    {
        if (JSON==nil) {
            return;
        }
        if([JSON isKindOfClass:[NSDictionary class]])
        {
            if (JSON[@"code"]!=0&&[JSON[@"code"]integerValue]!=200) {
                [ViewController hideHud];
                
                return ;
            }
        }
        if (block) {
            [ViewController hideHud];
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
        
        NSLog(@"%@",[task.response description]);
        
        if (block) {
            [ViewController hideHud];
            block(nil, error);
        }
    }];
    [UIAlertView showAlertViewForTaskWithErrorOnCompletion:task delegate:nil];
}

+ (void)PostRequstWithUrl:(NSString*)url
                                    andView:(id)vc
                                    andMSG:(NSString*)msg
                              andParameters:(NSMutableDictionary*)parameters
                                      Block:(void (^)(id objc, NSError *error))block
{
    UIViewController *ViewController = (UIViewController*)vc;
    [vc showHudInView:ViewController.view hint:msg];
    NSURLSessionDataTask *task = [[FPAppDotNetAPIClient sharedClient]POST:url
                                         parameters:parameters
                                            success:^(NSURLSessionDataTask * __unused task, id JSON){
                                            
                                            
                                                if ([JSON[@"code"]integerValue] !=0&&[JSON[@"code"]integerValue]!=200)
                                                {
                                                    [ViewController hideHud];
                                                    
                                                    return ;
                                                }
        if (block) {
            [ViewController hideHud];
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
        if (block) {
            [ViewController hideHud];
            block(nil, error);
        }
    }];
    /*UIActivityIndicatorView  *activityV = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [view addSubview:activityV];
    [activityV setAnimatingWithStateOfTask:task];*/
    [UIAlertView showAlertViewForTaskWithErrorOnCompletion:task delegate:nil];
}
+ (void)PostRequstWithUrl:(NSString*)url
                                    andView:(id)vc
                                    andMSG:(NSString*)msg
                                andIMGName:(NSString*)fileName
                andIsRealNameCertification:(BOOL)isRNC
                              andParameters:(NSMutableDictionary*)parameters
                                  andImages:(NSMutableArray*)imgs
                                      Block:(void (^)(id objc, NSError *error))block
{
    UIViewController *ViewController = (UIViewController*)vc;
    [vc showHudInView:ViewController.view hint:msg];
    NSURLSessionDataTask *task = [[FPAppDotNetAPIClient sharedClient]POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (isRNC) {
            NSData *idata = UIImageJPEGRepresentation(imgs[0], 1.0);
            [formData  appendPartWithFileData:idata name:[NSString stringWithFormat:@"%@.file",fileName] fileName:[NSString stringWithFormat:@"%@.jpg",fileName] mimeType:@"image/jpg"];
        }
        else
        {
            int i=0;
            for (FPUploadIMG *image in imgs)
            {
                NSData *idata = UIImageJPEGRepresentation(image.img, 1.0);
                [formData  appendPartWithFileData:idata name:[NSString stringWithFormat:@"pictures[%d].File",i] fileName:[NSString stringWithFormat:@"pictures[%d].jpg",i] mimeType:@"image/jpg"];
                i++;
            }
        }
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [ViewController hideHud];
        if (responseObject[@"code"]!=0&&[responseObject[@"code"]integerValue] !=200) {
            
            return ;
        }
        if (block) {
            block(responseObject, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [ViewController hideHud];
        if (block) {
            block(nil, error);
        }
    }];
    /*UIActivityIndicatorView  *activityV = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [view addSubview:activityV];
    [activityV setAnimatingWithStateOfTask:task];*/
    [UIAlertView showAlertViewForTaskWithErrorOnCompletion:task delegate:nil];
}
+ (AFHTTPRequestOperation *)postPostAddressWithUrl:(NSString *)url
                                     andAttributes:(NSMutableArray *)attributes
                                          andBlock:(void (^)(id response, NSError *error))block
{
    return [[FPAPPClient sharedClient]POST:url Array:attributes success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (block) {
            block(responseObject,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block) {
            block(nil,error);
        }
    }];
}

@end
