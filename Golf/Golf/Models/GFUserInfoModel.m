//
//  GFUserInfoModel.m
//  Golf
//
//  Created by 朱明 on 15/3/8.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFUserInfoModel.h"

@implementation GFUserInfoModel


- (instancetype)initWithDic:(NSDictionary *)dic
{
    self=[super initWithDictionary:dic error:nil];
    [self synchronousLocal];
    return self;
}



- (void)synchronousLocal
{
    [[NSUserDefaults standardUserDefaults] setObject:[self toDictionary] forKey:@"user"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)initWithLocal
{
    NSDictionary *pDic=[[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    self=[super initWithDictionary:pDic error:nil];
    return self;
}


@end
