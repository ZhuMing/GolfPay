//
//  GFCollectModel.h
//  Golf
//
//  Created by 朱明 on 15/3/11.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "BaseModel.h"

@interface GFCollectModel : BaseModel
@property (nonatomic,assign) NSInteger CollectibleType;
@property (nonatomic,strong) NSString *ImageUrl;
@property (nonatomic,strong) NSString *ItemTitle;
@property (nonatomic,strong) NSString *Id;
@property (nonatomic,strong) NSString *ItemId;
@property (nonatomic,strong) NSString *CustomerId;



@end
