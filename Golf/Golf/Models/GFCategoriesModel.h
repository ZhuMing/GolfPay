//
//  GFCategoriesModel.h
//  Golf
//
//  Created by 朱明 on 15/1/29.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GFCategoriesModel : NSObject

@property (nonatomic,strong) NSString *Id;
@property (nonatomic,strong) NSString *TypeName;

-(instancetype)initWithDic:(NSDictionary*)dic;


@end
