//
//  GFAPPClient.m
//  GolfFun
//
//  Created by 朱明 on 14-5-15.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import "FPAPPClient.h"

@implementation FPAPPClient

+ (instancetype)sharedClient {
    static FPAPPClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[FPAPPClient alloc] initWithBaseURL:[NSURL URLWithString:FPAppDotNetAPIBaseURLString]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    });
    return _sharedClient;
}

@end
