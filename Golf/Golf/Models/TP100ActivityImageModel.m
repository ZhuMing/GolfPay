//
//  activityImageModel.m
//  Golf
//
//  Created by zhouhong on 3/7/15.
//  Copyright (c) 2015 ZHU. All rights reserved.
//

#import "TP100ActivityImageModel.h"

@implementation TP100ActivityImageModel

-(instancetype)initWithDic:(NSDictionary*)dic
{
    if (self = [super init]) {
        self.ImgId = dic[@"Id"];
        self.ActivityId = dic[@"ActivityId"];
        self.ImageTitle = dic[@"ImageTitle"];
        self.ImageUrl = dic[@"ImageUrl"];
    }
    return self;
}


@end
