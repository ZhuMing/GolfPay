//
//  NSDictionary+NOTNULL.m
//  FindPeople
//
//  Created by 朱明 on 14/11/26.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import "NSDictionary+NOTNULL.h"

@implementation NSDictionary (NOTNULL)

-(NSArray*)ArrayForKey:(id)aKey
{
    if ([[self objectForKey:aKey]isKindOfClass:[NSNull class]]||[self objectForKey:aKey]==nil) {
        return [NSArray array];
    }
    return [self objectForKey:aKey];
}

-(NSDictionary*)DictionaryForKey:(id)aKey
{
    if ([[self objectForKey:aKey]isKindOfClass:[NSNull class]]||[self objectForKey:aKey]==nil) {
        return [NSDictionary dictionary];
    }
    return [self objectForKey:aKey];
}

-(NSString*)StringForKey:(id)aKey
{
    if ([[self objectForKey:aKey]isKindOfClass:[NSNull class]]||[self objectForKey:aKey]==nil) {
        return @"";
    }
    return [self objectForKey:aKey];
}

-(id)ObjectForKey:(id)aKey
{
    if ([[self objectForKey:aKey]isKindOfClass:[NSNull class]]) {
        return  [NSObject new];
    }
    return [self objectForKey:aKey];
}

@end
