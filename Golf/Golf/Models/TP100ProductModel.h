//
//  TP100ProductModel.h
//  Golf
//
//  Created by zhouhong on 3/7/15.
//  Copyright (c) 2015 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TP100ProductModel : NSObject

@property (nonatomic,strong) NSString *ProId;
@property (nonatomic,strong) NSString *ProductName;
@property (nonatomic,assign) NSInteger Price;
@property (nonatomic,assign) NSInteger Stock;
@property (nonatomic,assign) NSInteger Score;
@property (nonatomic,assign) NSInteger Sales;
@property (nonatomic,strong) NSString *Type;
@property (nonatomic,strong) NSString *CreateDate;
@property (nonatomic,strong) NSString *ActivityId;
@property (nonatomic,strong) NSString *ProductContent;
@property (nonatomic,strong) NSString *ATagName;
@property (nonatomic,strong) NSString *BTagName;
@property (nonatomic,strong) NSString *ITagName;
@property (nonatomic,strong) NSMutableArray *tagList;

-(instancetype)initWithDic:(NSDictionary*)dic;

@end
