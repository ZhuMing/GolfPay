//
//  activityImageModel.h
//  Golf
//
//  Created by zhouhong on 3/7/15.
//  Copyright (c) 2015 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TP100ActivityImageModel : NSObject

@property (nonatomic,strong) NSString *ImgId;
@property (nonatomic,strong) NSString *ActivityId;
@property (nonatomic,strong) NSString *ImageUrl;
@property (nonatomic,strong) NSString *ImageTitle;

-(instancetype)initWithDic:(NSDictionary*)dic;



@end
