//
//  GFActivityModel.m
//  Golf
//
//  Created by 朱明 on 15/1/29.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFActivityModel.h"

@implementation GFActivityModel
-(instancetype)initWithDic:(NSDictionary *)dic
{
    if (self = [super init])
    {
        NSLog(@"%@", dic[@"IndividualityTagName"]);
        self.IndividualityTagName = dic[@"IndividualityTagName"];
        self.ActivityName = dic[@"ActivityName"];
        
        
//        ../Resourse/images/Theme/TopicImg/Lighthouse(130700853773423809).jpg
        self.ITagName = dic[@"ITagName"];
        self.ATagName = dic[@"ATagName"];
        self.BTagName = dic[@"BTagName"];
        
        self.tagList = [NSMutableArray arrayWithCapacity:3];
        
        if (self.ATagName.length>0) {
            [self.tagList addObject:self.ATagName];
        }
        if (self.BTagName.length>0) {
            [self.tagList addObject:self.BTagName];
        }
        if (self.ITagName.length>0) {
            [self.tagList addObject:self.ITagName];
        }
        
        self.ActivityPhoto = dic[@"ActivityPhoto"];
        
        if (self.ActivityPhoto.length>0) {
            self.ActivityPhoto= [self.ActivityPhoto stringByReplacingOccurrencesOfString:@"../"withString:GFPotoBaseURLString];
        }
        self.AreaTagName = dic[@"AreaTagName"];
        self.Id = dic[@"Id"];
    }
    return self;
}
@end
