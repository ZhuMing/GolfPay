//
//  GFPerfectInformationHeaderView.m
//  Golf
//
//  Created by 朱明 on 15/2/5.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFPerfectInformationHeaderView.h"

@implementation GFPerfectInformationHeaderView
- (id) init {
    id obj = loadObjectFromNib(@"GFPerfectInformationHeaderView", [GFPerfectInformationHeaderView class], self);
    if (obj) {
        self = (GFPerfectInformationHeaderView *)obj;
    } else {
        self = [self init];
    }
    _userIconView.layer.masksToBounds = YES;
    _userIconView.layer.cornerRadius = _userIconView.frame.size.width/2;
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)changeIcon:(UIButton *)sender {
    if (_ChangeIconClike) {
        _ChangeIconClike();
    }
}
@end
