//
//  GFSearchResultsDHV.m
//  Golf
//
//  Created by 朱明 on 15/2/11.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFSearchResultsDHV.h"

@implementation GFSearchResultsDHV
- (id) init {
    id obj = loadObjectFromNib(@"GFSearchResultsDHV", [GFSearchResultsDHV class], self);
    if (obj) {
        self = (GFSearchResultsDHV *)obj;
    } else {
        self = [self init];
    }
    _userIocnView.layer.masksToBounds = YES;
    _userIocnView.layer.cornerRadius = _userIocnView.frame.size.width/2;
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)ClikeDHVBtn:(UIButton *)sender {
    if (_ClikeNavBtnAction) {
        _ClikeNavBtnAction(sender);
    }
}
@end
