//
//  GFMatchingServiceCell.h
//  Golf
//
//  Created by 朱明 on 15/2/2.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GFServerFootView.h"
#import "GFServerSubCell.h"
#import "TP100ProductModel.h"

@interface GFMatchingServiceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)GFServerFootView *footView;
//@property (nonatomic,strong)TP100ProductModel *model;
@property (nonatomic,strong)NSMutableArray *dataSource;

@property (nonatomic,copy) void (^footMore)();
@property (nonatomic,copy) void (^footGo)();
@property (nonatomic,copy) void (^footBeen)();
@property (nonatomic,copy) void (^footGoList)();
@property (nonatomic,copy) void (^footBeenList)();

@property (nonatomic,copy) void (^selectIndexCell)(GFMatchingServiceCell *cell,TP100ProductModel *model);

@end
