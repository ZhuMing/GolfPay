//
//  GFServerFootView.h
//  Golf
//
//  Created by 朱明 on 15/2/2.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFServerFootView : UIView

@property (nonatomic,copy) void (^more)();
@property (nonatomic,copy) void (^go)();
@property (nonatomic,copy) void (^been)();

@property (nonatomic,copy) void (^beenList)();

@property (nonatomic,copy) void (^goList)();

- (IBAction)GoList:(UIButton *)sender;

- (IBAction)beenList:(UIButton *)sender;




- (IBAction)moreClike:(UIButton *)sender;
- (IBAction)goBtn:(UIButton *)sender;
- (IBAction)beenBtn:(UIButton *)sender;

@end
