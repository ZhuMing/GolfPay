//
//  GFPerfectInformationHeaderView.h
//  Golf
//
//  Created by 朱明 on 15/2/5.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFPerfectInformationHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *userIconView;
@property (nonatomic,copy) void (^ChangeIconClike)();

- (IBAction)changeIcon:(UIButton *)sender;

@end
