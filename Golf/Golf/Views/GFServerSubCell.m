//
//  GFServerSubCell.m
//  Golf
//
//  Created by 朱明 on 15/2/2.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFServerSubCell.h"

@implementation GFServerSubCell
- (id) init {
    id obj = loadObjectFromNib(@"GFServerSubCell", [GFServerSubCell class], self);
    if (obj) {
        self = (GFServerSubCell *)obj;
    } else {
        self = [self init];
    }
    return self;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
