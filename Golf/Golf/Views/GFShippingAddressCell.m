//
//  GFShippingAddressCell.m
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFShippingAddressCell.h"

@implementation GFShippingAddressCell
- (id) init {
    id obj = loadObjectFromNib(@"GFShippingAddressCell", [GFShippingAddressCell class], self);
    if (obj) {
        self = (GFShippingAddressCell *)obj;
    } else {
        self = [self init];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
