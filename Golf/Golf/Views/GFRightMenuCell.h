//
//  GFRightMenuCell.h
//  Golf
//
//  Created by 朱明 on 15/1/29.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFRightMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *LogoView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
