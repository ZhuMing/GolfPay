//
//  GFLeftMenuCell.m
//  Golf
//
//  Created by 朱明 on 15/1/29.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFLeftMenuCell.h"

@implementation GFLeftMenuCell
- (id) init {
    id obj = loadObjectFromNib(@"GFLeftMenuCell", [GFLeftMenuCell class], self);
    if (obj) {
        self = (GFLeftMenuCell *)obj;
    } else {
        self = [self init];
    }
    
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
