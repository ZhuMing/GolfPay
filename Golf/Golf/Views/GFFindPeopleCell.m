//
//  GFFindPeopleCell.m
//  Golf
//
//  Created by 朱明 on 15/2/10.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFFindPeopleCell.h"
#import "GFUserInfoModel.h"

@implementation GFFindPeopleCell
- (id) init {
    id obj = loadObjectFromNib(@"GFFindPeopleCell", [GFFindPeopleCell class], self);
    if (obj) {
        self = (GFFindPeopleCell *)obj;
    } else {
        self = [self init];
    }
    _userIconView.layer.masksToBounds = YES;
    _userIconView.layer.cornerRadius = _userIconView.frame.size.width/2;
    return self;
}
- (IBAction)CellBtnClike:(UIButton *)sender {
    if (_cellBtnClike) {
        _cellBtnClike(self);
    }
}

-(void)configCellSubViewWith:(id)objc
{
    //赋值
    GFUserInfoModel *userModel = (GFUserInfoModel*)objc;
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@Resoures/Customer/%@",GFPotoBaseURLString,userModel.Logo]);
    
    [self.userIconView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://211.144.85.186:7850/Resoures/Customer/%@",userModel.Logo]] placeholderImage:[UIImage imageNamed:@"temp"]];
    self.userNameLabl.text = userModel.NikeName;
    [self.userNameLabl sizeToFit];
    self.userNameLabl.center = CGPointMake(self.userNameLabl.center.x, self.center.y);
    self.VipView.frame = CGRectMake(CGRectGetMaxX(self.userNameLabl.frame)+4, self.VipView.frame.origin.y, self.VipView.frame.size.width, self.VipView.frame.size.height);
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
