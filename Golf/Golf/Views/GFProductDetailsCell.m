//
//  GF Product Details Product Details Product Details GFProductDetailsCell.m
//  Golf
//
//  Created by 朱明 on 15/3/12.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFProductDetailsCell.h"

@implementation GFProductDetailsCell
- (id) init {
    id obj = loadObjectFromNib(@"GFProductDetailsCell", [GFProductDetailsCell class], self);
    if (obj) {
        self = (GFProductDetailsCell *)obj;
    } else {
        self = [self init];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
