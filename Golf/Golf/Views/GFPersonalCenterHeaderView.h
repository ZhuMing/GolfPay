//
//  GFPersonalCenterHeaderView.h
//  Golf
//
//  Created by 朱明 on 15/2/4.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFPersonalCenterHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *BGview;
@property (weak, nonatomic) IBOutlet UIImageView *userIconView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *viewCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *usertagListLabel;
@property (weak, nonatomic) IBOutlet UIButton *editInfoBtn;
- (IBAction)editInfoAction:(UIButton *)sender;

@end
