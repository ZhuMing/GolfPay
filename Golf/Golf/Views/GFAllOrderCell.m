//
//  GFAllOrderCell.m
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFAllOrderCell.h"

@implementation GFAllOrderCell

- (id) init {
    id obj = loadObjectFromNib(@"GFAllOrderCell", [GFAllOrderCell class], self);
    if (obj) {
        self = (GFAllOrderCell *)obj;
    } else {
        self = [self init];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)CellBtnAction:(UIButton *)sender {
    if (_CellAction) {
        _CellAction(self,sender);
    }
}
@end
