//
//  GFServerSubCell.h
//  Golf
//
//  Created by 朱明 on 15/2/2.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"
#import "DXStarRatingView.h"


@interface GFServerSubCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *PriceLabel;
@property (weak, nonatomic) IBOutlet DWTagList *tagView;
@property (weak, nonatomic) IBOutlet DXStarRatingView *startView;

@end
