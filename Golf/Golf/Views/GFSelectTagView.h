//
//  GFSelectTagView.h
//  Golf
//
//  Created by 朱明 on 15/2/8.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GFTagModel.h"

@interface GFSelectTagView : UIViewController

@property (weak, nonatomic) IBOutlet UIPickerView *TagPickerView;

@property (nonatomic,copy) void (^dismiss)();

@property (nonatomic,copy) void (^OK)(GFTagModel*model);

@property (nonatomic,assign) BOOL isArea;

- (IBAction)dismiss:(UIButton *)sender;
- (IBAction)okAction:(UIButton *)sender;

@end
