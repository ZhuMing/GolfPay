//
//  GFFindPeopleCell.h
//  Golf
//
//  Created by 朱明 on 15/2/10.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFFindPeopleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userIconView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabl;
@property (weak, nonatomic) IBOutlet UIImageView *VipView;
@property (weak, nonatomic) IBOutlet UIButton *stausBtn;
@property (weak, nonatomic) IBOutlet UIImageView *stausView;
@property (copy, nonatomic) void (^cellBtnClike)(GFFindPeopleCell*cell);

- (IBAction)CellBtnClike:(UIButton *)sender;
-(void)configCellSubViewWith:(id)objc;
@end
