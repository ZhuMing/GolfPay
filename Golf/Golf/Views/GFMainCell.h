//
//  GFMainCell.h
//  Golf
//
//  Created by 朱明 on 15/1/28.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"

@interface GFMainCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet DWTagList *tagList;
@property (weak, nonatomic) IBOutlet UIImageView *BackGroupImageView;
@property (weak, nonatomic) IBOutlet UILabel     *AddressLabel;
@property (weak, nonatomic) IBOutlet UILabel     *dateTimeLabel;


@end
