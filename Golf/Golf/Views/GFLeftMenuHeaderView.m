//
//  GFLeftMenuHeaderView.m
//  Golf
//
//  Created by 朱明 on 15/1/28.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFLeftMenuHeaderView.h"

@implementation GFLeftMenuHeaderView
- (id) init {
    id obj = loadObjectFromNib(@"GFLeftMenuHeaderView", [GFLeftMenuHeaderView class], self);
    if (obj) {
        self = (GFLeftMenuHeaderView *)obj;
    } else {
        self = [self init];
    }
    _userIconView.layer.masksToBounds = YES;
    _userIconView.layer.cornerRadius = _userIconView.frame.size.width/2;
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)touchImage:(UITapGestureRecognizer *)sender {
    if (_showLoginView) {
        _showLoginView(sender.view);
    }
}
@end
