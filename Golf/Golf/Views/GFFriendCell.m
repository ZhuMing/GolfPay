//
//  GFFriendCell.m
//  Golf
//
//  Created by 朱明 on 15/2/10.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFFriendCell.h"
#import "GFUserInfoModel.h"

@implementation GFFriendCell
- (id) init {
    id obj = loadObjectFromNib(@"GFFriendCell", [GFFriendCell class], self);
    if (obj) {
        self = (GFFriendCell *)obj;
    } else {
        self = [self init];
    }
    _userIconView.layer.masksToBounds = YES;
    _userIconView.layer.cornerRadius = _userIconView.frame.size.width/2;
    return self;
}
-(void)configCellSubViewWith:(id)objc
{
    //赋值
    
    GFUserInfoModel *model = (GFUserInfoModel*)objc;
    [self.userIconView setImageWithURL:[NSURL URLWithString:model.Logo] placeholderImage:[UIImage imageNamed:@"temp"]];
    self.userNameLabl.text = model.NikeName;
    [self.userNameLabl sizeToFit];
    self.userNameLabl.center = CGPointMake(self.userNameLabl.center.x, self.center.y);
    self.VipView.frame = CGRectMake(CGRectGetMaxX(self.userNameLabl.frame)+4, self.VipView.frame.origin.y, self.VipView.frame.size.width, self.VipView.frame.size.height);
    self.sexView.frame = CGRectMake(CGRectGetMaxX(self.VipView.frame)+4, self.sexView.frame.origin.y, self.sexView.frame.size.width, self.sexView.frame.size.height);
}

- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
