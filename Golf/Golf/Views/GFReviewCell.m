//
//  GFReviewCell.m
//  Golf
//
//  Created by 朱明 on 15/2/3.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFReviewCell.h"
#import "GFReviewPhotoCell.h"
@implementation GFReviewCell
- (id) init {
    id obj = loadObjectFromNib(@"GFReviewCell", [GFReviewCell class], self);
    if (obj) {
        self = (GFReviewCell *)obj;
    } else {
        self = [self init];
    }
        _userIconView.layer.masksToBounds = YES;
        _userIconView.layer.cornerRadius = _userIconView.frame.size.width/2;
    [self.CollectionView registerClass:[GFReviewPhotoCell class] forCellWithReuseIdentifier:@"GFReviewPhotoCell"];
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) configViewWithArr:(NSMutableArray *)arr
{
    _photos = arr;
    [_CollectionView reloadData];
    
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GFReviewPhotoCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GFReviewPhotoCell" forIndexPath:indexPath];
//    [cell.photoView setImageWithURL:[NSURL URLWithString:_photos[indexPath.row]] placeholderImage:[UIImage imageNamed:@"image"]];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
