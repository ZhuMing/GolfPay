//
//  GFReviewPhotoCell.h
//  Golf
//
//  Created by 朱明 on 15/2/3.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFReviewPhotoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoView;

@end
