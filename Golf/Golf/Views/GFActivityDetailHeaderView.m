//
//  GFActivityDetailHeaderView.m
//  Golf
//
//  Created by 朱明 on 15/1/31.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFActivityDetailHeaderView.h"

@implementation GFActivityDetailHeaderView
- (id) init {
    id obj = loadObjectFromNib(@"GFActivityDetailHeaderView", [GFActivityDetailHeaderView class], self);
    if (obj) {
        self = (GFActivityDetailHeaderView *)obj;
    } else {
        self = [self init];
    }
    [self.CollectionView registerClass:[GFDetailPhotoCell class] forCellWithReuseIdentifier:@"GFDetailPhotoCell"];
    return self;
}
-(void) configViewWithArr:(NSMutableArray *)arr
{
    _photos = arr;
    [_CollectionView reloadData];
    
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _photos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GFDetailPhotoCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GFDetailPhotoCell" forIndexPath:indexPath];
    [cell.imageView setImageWithURL:[NSURL URLWithString:_photos[indexPath.row]] placeholderImage:[UIImage imageNamed:@"image"]];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (IBAction)clike:(UIButton *)sender {
    if (_clikeNavBtn) {
        _clikeNavBtn(sender);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
