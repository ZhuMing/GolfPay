//
//  GFPerfectInformationFootCell.m
//  Golf
//
//  Created by 朱明 on 15/2/5.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFPerfectInformationFootCell.h"

@implementation GFPerfectInformationFootCell
- (id) init {
    id obj = loadObjectFromNib(@"GFPerfectInformationFootCell", [GFPerfectInformationFootCell class], self);
    if (obj) {
        self = (GFPerfectInformationFootCell *)obj;
    } else {
        self = [self init];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)saveAction:(UIButton *)sender {
    if (_saveInformation) {
        _saveInformation();
    }
}

- (IBAction)nextAction:(UIButton *)sender {
    if (_nextStep) {
        _nextStep();
    }
}
@end
