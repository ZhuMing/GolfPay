//
//  GF Product Details Product Details Product Details GFProductDetailsCell.h
//  Golf
//
//  Created by 朱明 on 15/3/12.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFProductDetailsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
