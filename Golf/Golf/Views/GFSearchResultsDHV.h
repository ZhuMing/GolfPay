//
//  GFSearchResultsDHV.h
//  Golf
//
//  Created by 朱明 on 15/2/11.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFSearchResultsDHV : UIView
@property (weak, nonatomic) IBOutlet UIImageView *SearchResultsBV;
@property (weak, nonatomic) IBOutlet UIImageView *userIocnView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *VipView;
@property (weak, nonatomic) IBOutlet UILabel *signLabel;
@property (nonatomic,copy) void (^ChangeIconClike)();

@property (nonatomic,copy) void (^ClikeNavBtnAction)(UIButton*sender);


- (IBAction)ClikeDHVBtn:(UIButton *)sender;


@end
