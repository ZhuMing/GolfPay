//
//  GFPerfectInformationCell.m
//  Golf
//
//  Created by 朱明 on 15/2/5.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFPerfectInformationCell.h"

@implementation GFPerfectInformationCell
- (id) init {
    id obj = loadObjectFromNib(@"GFPerfectInformationCell", [GFPerfectInformationCell class], self);
    if (obj) {
        self = (GFPerfectInformationCell *)obj;
    } else {
        self = [self init];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
