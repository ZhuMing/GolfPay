//
//  GFServerFootView.m
//  Golf
//
//  Created by 朱明 on 15/2/2.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFServerFootView.h"

@implementation GFServerFootView
- (id) init {
    id obj = loadObjectFromNib(@"GFServerFootView", [GFServerFootView class], self);
    if (obj) {
        self = (GFServerFootView *)obj;
    } else {
        self = [self init];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)GoList:(UIButton *)sender {
    if (_goList) {
        _goList();
    }
}

- (IBAction)beenList:(UIButton *)sender {
    if (_beenList) {
        _beenList();
    }
}

- (IBAction)moreClike:(UIButton *)sender {
    if (_more) {
        _more();
    }
}

- (IBAction)goBtn:(UIButton *)sender {
    if (_go) {
        _go();
    }
}

- (IBAction)beenBtn:(UIButton *)sender {
    if (_been) {
        _been();
    }
}
@end
