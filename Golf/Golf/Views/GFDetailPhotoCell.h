//
//  GFDetailPhotoCell.h
//  Golf
//
//  Created by 朱明 on 15/2/2.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFDetailPhotoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
