//
//  GFAllOrderCell.h
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFAllOrderCell : UITableViewCell


@property (nonatomic,copy) void (^CellAction) (GFAllOrderCell *cell,UIButton*btn);

- (IBAction)CellBtnAction:(UIButton *)sender;

@end
