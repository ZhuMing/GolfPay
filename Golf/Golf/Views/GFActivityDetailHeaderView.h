//
//  GFActivityDetailHeaderView.h
//  Golf
//
//  Created by 朱明 on 15/1/31.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GFDetailPhotoCell.h"

@interface GFActivityDetailHeaderView : UIView
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *AreaLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;
@property (nonatomic,strong) NSMutableArray *photos;
@property (nonatomic,copy) void (^clikeNavBtn)(UIButton*Sender);

-(void) configViewWithArr:(NSMutableArray *)arr;

@end
