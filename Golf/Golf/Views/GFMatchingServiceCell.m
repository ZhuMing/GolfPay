//
//  GFMatchingServiceCell.m
//  Golf
//
//  Created by 朱明 on 15/2/2.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFMatchingServiceCell.h"

@implementation GFMatchingServiceCell
- (id) init {
    id obj = loadObjectFromNib(@"GFMatchingServiceCell", [GFMatchingServiceCell class], self);
    if (obj) {
        self = (GFMatchingServiceCell *)obj;
    } else {
        self = [self init];
    }
    _footView = [[GFServerFootView alloc]init];
    //查看更多
    __weak typeof(self) WeakSelf = self;
    
    [_footView setMore:^{
        if(WeakSelf.footMore)
        {
            WeakSelf.footMore();
        }
    }];
    //想去
    [_footView setGo:^{
        if(WeakSelf.footGo)
        {
            WeakSelf.footGo();
        }
    }];
    //去过
    [_footView setBeen:^{
        if(WeakSelf.footBeen)
        {
            WeakSelf.footBeen();
        }
    }];
    //想去列表
    [_footView setGoList:^{
        if(WeakSelf.footGoList)
        {
            WeakSelf.footGoList();
        }
    }];
    //去过列表
    [_footView setBeenList:^{
        if(WeakSelf.footBeenList)
        {
            WeakSelf.footBeenList();
        }
    }];
    self.tableView.tableFooterView = _footView;
    return self;
}



- (void)awakeFromNib {
    // Initialization code
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GFServerSubCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[GFServerSubCell alloc]init];
        
    }
    
    TP100ProductModel *model = self.dataSource[indexPath.row];
    cell.titleLabel.text = model.ProductName;
    cell.PriceLabel.text = [NSString stringWithFormat:@"¥ %li",(long)model.Price];
    [cell.tagView setTags:model.tagList];
    [cell.startView setStars:model.Score callbackBlock:^(NSNumber *newRating) {
    }];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_selectIndexCell) {
        _selectIndexCell(self,self.dataSource[indexPath.row]);
    }
    
}
@end
