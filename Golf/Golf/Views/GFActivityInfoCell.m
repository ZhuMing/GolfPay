//
//  GFActivityInfoCell.m
//  Golf
//
//  Created by 朱明 on 15/2/2.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFActivityInfoCell.h"

@implementation GFActivityInfoCell
- (id) init {
    id obj = loadObjectFromNib(@"GFActivityInfoCell", [GFActivityInfoCell class], self);
    if (obj) {
        self = (GFActivityInfoCell *)obj;
    } else {
        self = [self init];
    }
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
