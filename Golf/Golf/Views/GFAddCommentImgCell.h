//
//  GFAddCommentImgCell.h
//  Golf
//
//  Created by 朱明 on 15/2/7.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFAddCommentImgCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end
