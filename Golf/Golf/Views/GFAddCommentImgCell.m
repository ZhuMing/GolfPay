//
//  GFAddCommentImgCell.m
//  Golf
//
//  Created by 朱明 on 15/2/7.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFAddCommentImgCell.h"

@implementation GFAddCommentImgCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"GFAddCommentImgCell" owner:self options: nil];
        // 如果路径不存在，return nil
        if(arrayOfViews.count < 1){return nil;}
        // 如果xib中view不属于UICollectionViewCell类，return nil
        if(![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]])
        {
            return nil;
        }
        // 加载nib
        self = [arrayOfViews objectAtIndex:0];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _imgView.layer.masksToBounds = YES;
    _imgView.layer.cornerRadius = _imgView.frame.size.width/2;
    // Initialization code
}

@end
