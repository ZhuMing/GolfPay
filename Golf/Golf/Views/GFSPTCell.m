//
//  GFSPTCell.m
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFSPTCell.h"

@implementation GFSPTCell
- (id) init {
    id obj = loadObjectFromNib(@"GFSPTCell", [GFSPTCell class], self);
    if (obj) {
        self = (GFSPTCell *)obj;
    } else {
        self = [self init];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
