//
//  GFReviewSecionView.m
//  Golf
//
//  Created by 朱明 on 15/2/3.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFReviewSecionView.h"

@implementation GFReviewSecionView
- (id) init {
    id obj = loadObjectFromNib(@"GFReviewSecionView", [GFReviewSecionView class], self);
    if (obj) {
        self = (GFReviewSecionView *)obj;
    } else {
        self = [self init];
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
