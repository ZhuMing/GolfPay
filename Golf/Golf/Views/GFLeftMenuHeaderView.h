//
//  GFLeftMenuHeaderView.h
//  Golf
//
//  Created by 朱明 on 15/1/28.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFLeftMenuHeaderView : UIView
@property(nonatomic,copy) void (^showLoginView)(id objc);
@property (weak, nonatomic) IBOutlet UIImageView *userIconView;
@property (weak, nonatomic) IBOutlet UILabel     *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel     *userInfoLabel;

- (IBAction)touchImage:(UITapGestureRecognizer *)sender;

@end
