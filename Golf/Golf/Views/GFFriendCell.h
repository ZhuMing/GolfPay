//
//  GFFriendCell.h
//  Golf
//
//  Created by 朱明 on 15/2/10.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFFriendCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userIconView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabl;
@property (weak, nonatomic) IBOutlet UIImageView *VipView;
@property (weak, nonatomic) IBOutlet UIImageView *sexView;
-(void)configCellSubViewWith:(id)objc;

@end
