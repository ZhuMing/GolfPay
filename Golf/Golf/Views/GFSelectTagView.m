//
//  GFSelectTagView.m
//  Golf
//
//  Created by 朱明 on 15/2/8.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFSelectTagView.h"


@interface GFSelectTagView ()
@property (nonatomic,strong) NSMutableArray *areaArr;
@end

@implementation GFSelectTagView
- (void)loadDataArea
{
    [FPApiRequst getRequstWithUrl:_isArea?Customer_GetAreaTag:Customer_GetIndividualityTag andView:nil andMSG:nil andParameters:nil Block:^(id objc, NSError *error) {
        if (!error) {
            _areaArr = [NSMutableArray array];
            for (NSDictionary *dic in objc) {
                GFTagModel *model = [[GFTagModel alloc]initWithDic:dic];
                [_areaArr addObject:model];
            }
            [_TagPickerView reloadAllComponents];
        }
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadDataArea];
    
    // Do any additional setup after loading the view from its nib.
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _areaArr.count;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    GFTagModel *model = _areaArr[row];
    return model.TagName;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

- (IBAction)dismiss:(UIButton *)sender {
    if (_dismiss) {
        _dismiss();
    }
}

- (IBAction)okAction:(UIButton *)sender {
    NSInteger row = [self.TagPickerView selectedRowInComponent:0];
    if (_OK) {
        _OK(_areaArr[row]);
    }
}
@end
