//
//  GFRightMenuHeaderView.m
//  Golf
//
//  Created by 朱明 on 15/1/29.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFRightMenuHeaderView.h"

@implementation GFRightMenuHeaderView
- (id) init {
    id obj = loadObjectFromNib(@"GFRightMenuHeaderView", [GFRightMenuHeaderView class], self);
    if (obj) {
        self = (GFRightMenuHeaderView *)obj;
    } else {
        self = [self init];
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
