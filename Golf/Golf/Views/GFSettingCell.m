//
//  GFSettingCell.m
//  Golf
//
//  Created by 朱明 on 15/2/7.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFSettingCell.h"

@implementation GFSettingCell
- (id) init {
    id obj = loadObjectFromNib(@"GFSettingCell", [GFSettingCell class], self);
    if (obj) {
        self = (GFSettingCell *)obj;
    } else {
        self = [self init];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
