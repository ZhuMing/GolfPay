//
//  GFPerfectInformationFootCell.h
//  Golf
//
//  Created by 朱明 on 15/2/5.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFPerfectInformationFootCell : UITableViewCell
@property (nonatomic,strong) void (^saveInformation)();
@property (nonatomic,strong) void (^nextStep)();


- (IBAction)saveAction:(UIButton *)sender;
- (IBAction)nextAction:(UIButton *)sender;

@end
