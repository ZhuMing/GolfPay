//
//  GFRightMenuCell.m
//  Golf
//
//  Created by 朱明 on 15/1/29.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFRightMenuCell.h"

@implementation GFRightMenuCell
- (id) init {
    id obj = loadObjectFromNib(@"GFRightMenuCell", [GFRightMenuCell class], self);
    if (obj) {
        self = (GFRightMenuCell *)obj;
    } else {
        self = [self init];
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
