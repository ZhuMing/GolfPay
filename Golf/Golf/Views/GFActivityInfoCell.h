//
//  GFActivityInfoCell.h
//  Golf
//
//  Created by 朱明 on 15/2/2.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFActivityInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
