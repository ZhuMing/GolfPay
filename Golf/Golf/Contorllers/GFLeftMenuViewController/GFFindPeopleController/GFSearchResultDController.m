//
//  GFSearchResultController.m
//  Golf
//
//  Created by 朱明 on 15/2/11.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFSearchResultDController.h"
#import "GFSearchResultsDHV.h"
#import "GFPerfectInformationCell.h"

@interface GFSearchResultDController ()
@property (nonatomic,strong)NSArray *dataList;

@property (nonatomic,strong)GFSearchResultsDHV *headerView;


@end

@implementation GFSearchResultDController

- (void)viewDidLoad {
    [super viewDidLoad];
    _headerView = [[GFSearchResultsDHV alloc]init];
    self.tableView.tableHeaderView = _headerView;
    
    __weak typeof(self) WeakSelf = self;
    [_headerView setClikeNavBtnAction:^(UIButton *sender) {
        switch (sender.tag) {
            case 100:
            {
                [WeakSelf.navigationController popViewControllerAnimated:YES];
            }
                break;
            case 101:
            {
                //＋
                
            }
                break;
            default:
                break;
        }
    }];
    _dataList = @[@{@"leftImage":@"XB",@"content":@"性别"},@{@"leftImage":@"NL",@"content":@"年龄"},@{@"leftImage":@"CJD",@"content":@"常居地"},@{@"leftImage":@"HBQ",@"content":@"个性标签"}];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}




#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GFPerfectInformationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[GFPerfectInformationCell alloc]init];
    }
    NSDictionary *Dic = _dataList[indexPath.row];
    cell.titleLabel.text = Dic[@"content"];
    cell.iconView.image = [UIImage imageNamed:Dic[@"leftImage"]];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

- (IBAction)ClikeAction:(UIButton *)sender {
    switch (sender.tag) {
        case 100:
        {
            //加好友
            
        }
            break;
        case 101:
        {
            //已发表评论
            
        }
            break;
        case 102:
        {
            //举报
            
        }
            break;
        default:
            break;
    }
}
@end
