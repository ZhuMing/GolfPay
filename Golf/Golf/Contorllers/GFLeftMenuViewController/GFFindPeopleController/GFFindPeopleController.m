//
//  GFFindPeopleController.m
//  Golf
//
//  Created by 朱明 on 15/2/3.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFFindPeopleController.h"
#import "GFSearchResultsController.h"
#import "GFTagModel.h"
#import "GFUserInfoModel.h"

@interface GFFindPeopleController ()<UITableViewDelegate>
@property (nonatomic,strong)NSArray *SexArr;
@property (nonatomic,strong)NSMutableArray *AreaArr;
@property (nonatomic,strong)NSMutableArray *TypeArr;

@property (nonatomic,strong) NSMutableDictionary *Parameters;
@end

@implementation GFFindPeopleController



-(void)loadDataArea
{
    
    [FPApiRequst getRequstWithUrl:Customer_GetAreaTag andView:nil andMSG:nil andParameters:nil Block:^(id objc, NSError *error) {
        if (!error) {
            _AreaArr = [NSMutableArray array];
            for (NSDictionary *dic in objc) {
                GFTagModel *model = [[GFTagModel alloc]initWithDic:dic];
                [_AreaArr addObject:model];
            }
            if(_AreaArr.count>0)
            {
                [_areaBtn setTitle:((GFTagModel*)_AreaArr[0]).TagName forState:UIControlStateNormal];
                [self.Parameters setObject:((GFTagModel*)_AreaArr[0]).Id forKey:@"AreaId"];
            }
            
            [_areaTableView reloadData];
        }
    }];
}

-(void)loadTypeArea
{
    [FPApiRequst getRequstWithUrl:@"Customer/GetIndividualityTag" andView:nil andMSG:nil andParameters:nil Block:^(id objc, NSError *error) {
        if (!error) {
            _TypeArr = [NSMutableArray array];
            for (NSDictionary *dic in objc) {
                GFTagModel *model = [[GFTagModel alloc]initWithDic:dic];
                [_TypeArr addObject:model];
            }
            if(_TypeArr.count>0)
            {
                [_typeBtn setTitle:((GFTagModel*)_TypeArr[0]).TagName forState:UIControlStateNormal] ;
                [self.Parameters setObject:((GFTagModel*)_TypeArr[0]).Id forKey:@"IndividualityId"];
            }
            [_typeTableView reloadData];
        }
    }];
}

-(void)ShowleftMenu
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}
-(void)ShowRightMenu
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionRight animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.Parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"nikeName":@""}];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {  self.edgesForExtendedLayout = UIRectEdgeNone;}
    [FPTools setNavBar:self andTitle:@"全球找人"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(ShowleftMenu) RightBtnWithImageFile:@"FPHome" orRightTitle:nil andSEL:@selector(ShowRightMenu)];
    _SexArr = @[@"男",@"女",@"不限"];
    [self loadDataArea];
    [self loadTypeArea];
    [_SexBtn setTitle:_SexArr[0] forState:UIControlStateNormal] ;
     [self.Parameters setObject:[_SexArr[0] isEqualToString:@"男"]?@"1":@"0" forKey:@"Sex"];
    [_NcikNameText addTarget:self action:@selector(setSearchNcikName:) forControlEvents:UIControlEventEditingChanged];
//    _AreaArr = @[@"上海",@"广州",@"北京"];
//    _TypeArr = @[@"去马尔代夫",@"打高尔夫",@"去马尔代夫打高尔夫"];
}
-(void)setSearchNcikName:(UITextField*)textField
{
    [self.Parameters setObject:textField.text forKey:@"nikeName"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (tableView.tag) {
        case 100:
        {
            //性别
            return _SexArr.count;
        }
            break;
        case 101:
        {
            //地区
            return _AreaArr.count;
        }
            break;
        case 102:
        {
            //标题
            return _TypeArr.count;
        }
            break;
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuseIdentifier"];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    switch (tableView.tag) {
        case 100:
        {
            //性别
            cell.textLabel.text = _SexArr[indexPath.row];
        }
            break;
        case 101:
        {
            //地区
            cell.textLabel.text = ((GFTagModel*)_AreaArr[indexPath.row]).TagName;
        }
            break;
        case 102:
        {
            //标题
            cell.textLabel.text = ((GFTagModel*)_TypeArr[indexPath.row]).TagName;
        }
            break;
        default:
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
        case 100:
        {
            //性别
            [_SexBtn setTitle:_SexArr[indexPath.row] forState:UIControlStateNormal] ;
            
            [self.Parameters setObject:[_SexArr[indexPath.row] isEqualToString:@"男"]?@"1":@"0" forKey:@"Sex"];
            [_SexTableView setHidden:YES];
        }
            break;
        case 101:
        {
            //地区
            [_areaBtn setTitle:((GFTagModel*)_AreaArr[indexPath.row]).TagName forState:UIControlStateNormal];
            [self.Parameters setObject:((GFTagModel*)_AreaArr[indexPath.row]).Id forKey:@"AreaId"];
            [_areaTableView setHidden:YES];
            
        }
            break;
        case 102:
        {
            //标题
            [_typeBtn setTitle:((GFTagModel*)_TypeArr[indexPath.row]).TagName forState:UIControlStateNormal] ;
            [self.Parameters setObject:((GFTagModel*)_TypeArr[indexPath.row]).Id forKey:@"IndividualityId"];
            [_typeTableView setHidden:YES];
        }
            break;
        default:
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 20;
}
- (IBAction)selectSex:(UIButton *)sender {
    [_SexTableView setHidden:NO];
    [_areaTableView setHidden:YES];
    [_typeTableView setHidden:YES];
}

- (IBAction)selectArea:(UIButton *)sender {
    [_areaTableView setHidden:NO];
    [_SexTableView setHidden:YES];
    [_typeTableView setHidden:YES];
}

- (IBAction)selectType:(UIButton *)sender {
    [_typeTableView setHidden:NO];
    [_SexTableView setHidden:YES];
    [_areaTableView setHidden:YES];
}

- (IBAction)searchClike:(UIButton *)sender {
    
    [FPApiRequst getRequstWithUrl:Search_SearchPerson andView:self andMSG:@"搜索中..." andParameters:self.Parameters Block:^(id objc, NSError *error) {
        if (!error) {
            
            if ([objc[@"Total"]integerValue]<=0) {
                [self showHint:@"没有找到匹配的人！"];
                return ;
            }
            
            GFSearchResultsController *resultsC = [[GFSearchResultsController alloc]init];
            NSMutableArray *results = [NSMutableArray array];
            for (NSDictionary *item in objc[@"Data"]) {
                [results addObject:[[GFUserInfoModel alloc]initWithDictionary:item error:nil]];
            }
                     
            
            resultsC.SearchResultsArr = results;
            [self.navigationController pushViewController:resultsC animated:YES];
        }
    }];
    
    
    
}
@end
