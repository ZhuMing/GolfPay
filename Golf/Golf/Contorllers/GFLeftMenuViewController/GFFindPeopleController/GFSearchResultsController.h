//
//  GFSearchResultsController.h
//  Golf
//
//  Created by 朱明 on 15/2/10.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFSearchResultsController : UITableViewController

@property (nonatomic,strong)NSMutableArray *SearchResultsArr;


@end
