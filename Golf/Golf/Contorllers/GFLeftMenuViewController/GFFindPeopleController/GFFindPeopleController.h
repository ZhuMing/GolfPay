//
//  GFFindPeopleController.h
//  Golf
//
//  Created by 朱明 on 15/2/3.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFFindPeopleController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *SexBtn;
@property (weak, nonatomic) IBOutlet UITableView *SexTableView;
@property (weak, nonatomic) IBOutlet UIButton *areaBtn;
@property (weak, nonatomic) IBOutlet UITableView *areaTableView;
@property (weak, nonatomic) IBOutlet UIButton *typeBtn;
@property (weak, nonatomic) IBOutlet UITableView *typeTableView;
@property (weak, nonatomic) IBOutlet UITextField *NcikNameText;

- (IBAction)selectSex:(UIButton *)sender;
- (IBAction)selectArea:(UIButton *)sender;
- (IBAction)selectType:(UIButton *)sender;
- (IBAction)searchClike:(UIButton *)sender;


@end
