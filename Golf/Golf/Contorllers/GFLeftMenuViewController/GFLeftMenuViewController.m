//
//  GFLeftMenuViewController.m
//  Golf
//
//  Created by 朱明 on 15/1/28.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFLeftMenuViewController.h"
#import "GFLoginViewController.h"
#import "GFFindPeopleController.h"
#import "GFMainViewController.h"
#import "GFPersonalCenterController.h"
#import "GFSettingController.h"


#import "GFLeftMenuHeaderView.h"
#import "GFLeftMenuCell.h"


#define IMG @"image"
#define TITLE @"title"

@interface GFLeftMenuViewController ()
@property (nonatomic,strong) NSMutableArray *dataList;
@property (nonatomic,strong) GFLeftMenuHeaderView *HeaderView;

@end

@implementation GFLeftMenuViewController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    GFUserInfoModel *userModel = [[GFUserInfoModel alloc]initWithLocal];
    if (userModel.Id.length>0) {
        [self.HeaderView.userIconView setImageWithURL:[NSURL URLWithString:userModel.Logo] placeholderImage:[UIImage imageNamed:@"LoginIcon"]];
        self.HeaderView.userNameLabel.text = userModel.NikeName;
    }
    else
    {
        self.HeaderView.userIconView.image = [UIImage imageNamed:@"LoginIcon"];
        self.HeaderView.userNameLabel.text = @"登录";
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    __weak typeof(self) WeakSelf = self;
    
   _HeaderView = [[GFLeftMenuHeaderView alloc]init];
    self.tableView.tableHeaderView = _HeaderView;
    
    
    
    [_HeaderView setShowLoginView:^(id objc) {
        
        GFUserInfoModel *userModel = [[GFUserInfoModel alloc]initWithLocal];
        if (userModel.Id.length>0) {
            return ;
        }
        GFLoginViewController *loginVC = [[GFLoginViewController alloc]init];
        
        [loginVC setLoginSucess:^(GFUserInfoModel *userModel) {
            
            [WeakSelf.HeaderView.userIconView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://211.144.85.186:7850/Resoures/Customer/%@",userModel.Logo]] placeholderImage:[UIImage imageNamed:@"LoginIcon"]];
            WeakSelf.HeaderView.userNameLabel.text = userModel.NikeName;
//            WeakSelf.HeaderView.userInfoLabel.text = userModel;
            
        }];
        [WeakSelf presentViewController:loginVC animated:YES completion:nil];
    }];
    self.tableView.tableFooterView = _footView;
    _dataList = [NSMutableArray arrayWithArray:@[@{IMG:@"top100",TITLE:@"top100"},@{IMG:@"QQZR",TITLE:@"全球找人"},@{IMG:@"QQZS",TITLE:@"全球找事"},@{IMG:@"ZSJ",TITLE:@"找商家"},@{IMG:@"GRZX",TITLE:@"个人中心"}]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GFLeftMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[GFLeftMenuCell alloc]init];
    }
    NSDictionary *dic = _dataList[indexPath.row];
    cell.titleLabel.text = dic[TITLE];
    cell.iconView.image = [UIImage imageNamed:dic[IMG]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            //top100
            GFMainViewController *MainVC = [[GFMainViewController alloc]init];
            MainVC.type = Top100;
             MainVC.title = @"top 100";
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:MainVC];
            [self.revealSideViewController popViewControllerWithNewCenterController:nav animated:YES];
        }
            break;
        case 1:
        {
           //全球找人
            GFFindPeopleController *fpc = [[GFFindPeopleController alloc]init];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:fpc];
            [self.revealSideViewController popViewControllerWithNewCenterController:nav animated:YES];
        }
            break;
        case 2:
        {
            //全球找事
            GFMainViewController *MainVC = [[GFMainViewController alloc]init];
            MainVC.type = LookForAJob;
            MainVC.title = @"全球找事";
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:MainVC];
            [self.revealSideViewController popViewControllerWithNewCenterController:nav animated:YES];
        }
            break;
        case 3:
        {
            //找商家
            GFMainViewController *MainVC = [[GFMainViewController alloc]init];
            MainVC.type = Trades;
            MainVC.title = @"找商家";
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:MainVC];
            [self.revealSideViewController popViewControllerWithNewCenterController:nav animated:YES];
        }
            break;
        case 4:
        {
            //个人中心
            GFPersonalCenterController *fpc = [[GFPersonalCenterController alloc]init];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:fpc];
            [self.revealSideViewController popViewControllerWithNewCenterController:nav animated:YES];
        }
            break;
        default:
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)SettingAction:(UIButton *)sender {
    
    GFSettingController *settingC = [[GFSettingController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:settingC];
    [self.revealSideViewController popViewControllerWithNewCenterController:nav animated:YES];
}
@end
