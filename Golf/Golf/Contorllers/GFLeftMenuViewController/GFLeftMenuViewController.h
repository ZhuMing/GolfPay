//
//  GFLeftMenuViewController.h
//  Golf
//
//  Created by 朱明 on 15/1/28.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFLeftMenuViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UIView *footView;
- (IBAction)SettingAction:(UIButton *)sender;

@end
