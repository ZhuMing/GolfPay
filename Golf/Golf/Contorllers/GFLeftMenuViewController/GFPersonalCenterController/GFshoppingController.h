//
//  GFshoppingController.h
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFshoppingController : UIViewController
- (IBAction)AddAddress:(UIButton *)sender;
- (IBAction)payType:(UIButton *)sender;
- (IBAction)subMit:(UIButton *)sender;

@end
