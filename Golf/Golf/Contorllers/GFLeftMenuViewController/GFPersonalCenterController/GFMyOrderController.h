//
//  GFMyOrderController.h
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFMyOrderController : UITableViewController
@property (strong, nonatomic) IBOutlet UIView *HeaderView;
@property (weak, nonatomic) IBOutlet UIButton *AllBtn;
@property (weak, nonatomic) IBOutlet UIButton *DPJBtn;
@property (weak, nonatomic) IBOutlet UIButton *DFKBtn;
- (IBAction)HeaderViewClike:(UIButton *)sender;


@end
