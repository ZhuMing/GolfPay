//
//  GFSendMessageViewController.h
//  GolfFun
//
//  Created by 朱明 on 14/10/31.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import "JSQMessagesViewController.h"

#import "JSQMessages.h"





@interface GFSendMessageViewController : JSQMessagesViewController



@property (nonatomic,strong) NSMutableArray *messages;

@property (nonatomic,strong) UIImage *MeIcon;
@property (nonatomic,strong) UIImage *FriendIcon;

@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;


- (void)receiveMessagePressed:(UIBarButtonItem *)sender;

- (void)closePressed:(UIBarButtonItem *)sender;

@end
