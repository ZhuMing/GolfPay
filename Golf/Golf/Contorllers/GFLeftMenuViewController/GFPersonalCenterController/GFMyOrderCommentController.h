//
//  GFMyOrderCommentController.h
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXStarRatingView.h"

@interface GFMyOrderCommentController : UIViewController
@property (weak, nonatomic) IBOutlet DXStarRatingView *PLSJ_Star_View;
@property (weak, nonatomic) IBOutlet DXStarRatingView *PLSP_Star_View;
@property (weak, nonatomic) IBOutlet DXStarRatingView *MYCD_Star_View;
@property (weak, nonatomic) IBOutlet UIImageView *SJImageView;
@property (weak, nonatomic) IBOutlet UILabel *SPNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *SPJGLabel;

- (IBAction)subMit:(UIButton *)sender;

@end
