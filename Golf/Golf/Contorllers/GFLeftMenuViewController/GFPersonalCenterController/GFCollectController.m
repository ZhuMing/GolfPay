//
//  GFCollectController.m
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFCollectController.h"
#import "GFTop100Cell.h"
#import "GFSPTCell.h"
#import "GFSJCell.h"

#import "GFCollectModel.h"


typedef enum Htype
{
    TOP = 0,
    SP = 1,
    SJ = 2,
}CellType;

@interface GFCollectController ()
{
    CellType type;
}
@property (nonatomic,strong) NSMutableArray *dataList;


@end

@implementation GFCollectController
-(void)ShowleftMenu
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadData
{
    
    NSString *url = nil;
    switch (type) {
        case TOP:
        {
            url= [NSString stringWithFormat:@"Customer/GetFavoritesActivity/%@",_userModel.Id];
        }
            break;
        case SP:
        {
            url= [NSString stringWithFormat:@"Customer/GetFavoritesShop/%@",_userModel.Id];
        }
            break;
        case SJ:
        {
            url= [NSString stringWithFormat:@"Customer/GetFavoritesMerchandise/%@",_userModel.Id];
        }
            break;
        default:
            break;
    }
    
    [FPApiRequst getRequstWithUrl:url andView:self andMSG:@"加载中..." andParameters:@{@"CustomerId":_userModel.Id} Block:^(id objc, NSError *error) {
        if (!error) {
            _dataList = [NSMutableArray array];
            
            for (NSDictionary *dic in objc) {
                [_dataList addObject:[[GFCollectModel alloc]initWithDictionary:dic error:nil]];
            }
            
            [self.tableView reloadData];
        }
    }];
    
}

-(void)ShowRightMenu
{
    //add
}
- (IBAction)HeaderViewBtnAction:(UIButton *)sender {
    switch (sender.tag) {
        case 1000:
        {
            type = TOP;
            [_Top100Btn setTitleColor:[UIColor colorWithRed:241.0/255.0 green:62.0/255.0 blue:76.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_top100Line setHidden:NO];
            [_SJBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_SJLine setHidden:YES];
            [_SPBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_SPLine setHidden:YES];
            
        }
            break;
        case 1001:
        {
            type = SP;
            [_Top100Btn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_top100Line setHidden:YES];
            [_SJBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_SJLine setHidden:YES];
            [_SPBtn setTitleColor:[UIColor colorWithRed:241.0/255.0 green:62.0/255.0 blue:76.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_SPLine setHidden:NO];
        }
            break;
        case 1002:
        {
            type = SJ;
            [_Top100Btn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_top100Line setHidden:YES];
            [_SJBtn setTitleColor:[UIColor colorWithRed:241.0/255.0 green:62.0/255.0 blue:76.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_SJLine setHidden:NO];
            [_SPBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_SPLine setHidden:YES];
        }
            break;
        default:
            break;
    }
    
    [self loadData];
}




- (void)viewDidLoad {
    [super viewDidLoad];
    type = TOP;
    
    [FPTools setNavBar:self andTitle:@"全球收藏"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(ShowleftMenu) RightBtnWithImageFile:@"FPHome" orRightTitle:nil andSEL:@selector(ShowRightMenu)];
    [self loadData];
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return _HeaderView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return _dataList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (type) {
        case TOP:
        {
            GFTop100Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
            if (!cell) {
                cell = [[GFTop100Cell alloc]init];
            }
            
            
            // Configure the cell...
            
            return cell;
        }
            break;
        case SP:
        {
            GFSPTCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
            if (!cell) {
                cell = [[GFSPTCell alloc]init];
            }
            // Configure the cell...
            
            return cell;
        }
            break;
        case SJ:
        {
            GFSJCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
            if (!cell) {
                cell = [[GFSJCell alloc]init];
            }
            // Configure the cell...
            
            return cell;
        }
            break;
        default:
            break;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    
    // Configure the cell...
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 66.0;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
