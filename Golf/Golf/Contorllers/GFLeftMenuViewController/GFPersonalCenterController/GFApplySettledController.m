//
//  GFApplySettledController.m
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFApplySettledController.h"

@interface GFApplySettledController ()

@end

@implementation GFApplySettledController
-(void)ShowleftMenu
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)ShowRightMenu
{
    //add
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [FPTools setNavBar:self andTitle:@"申请入驻"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(ShowleftMenu) RightBtnWithImageFile:@"FPHome" orRightTitle:nil andSEL:@selector(ShowRightMenu)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
