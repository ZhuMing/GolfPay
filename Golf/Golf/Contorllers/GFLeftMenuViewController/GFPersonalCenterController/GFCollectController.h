//
//  GFCollectController.h
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GFUserInfoModel.h"

@interface GFCollectController : UITableViewController
@property (strong, nonatomic) IBOutlet UIView *HeaderView;
@property (weak, nonatomic) IBOutlet UIButton *Top100Btn;
@property (weak, nonatomic) IBOutlet UIButton *SJBtn;
@property (weak, nonatomic) IBOutlet UIButton *SPBtn;
@property (weak, nonatomic) IBOutlet UIView   *top100Line;
@property (weak, nonatomic) IBOutlet UIView   *SPLine;
@property (weak, nonatomic) IBOutlet UIView   *SJLine;
@property (nonatomic,strong) GFUserInfoModel *userModel;
@end
