//
//  GFshoppingController.m
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFshoppingController.h"
#import "GFShippingAddressController.h"
#import "GFPayTypeController.h"

@interface GFshoppingController ()

@end

@implementation GFshoppingController
-(void)ShowleftMenu
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)ShowRightMenu
{
    //add
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [FPTools setNavBar:self andTitle:@"购买订单"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(ShowleftMenu) RightBtnWithImageFile:@"FPHome" orRightTitle:nil andSEL:@selector(ShowRightMenu)];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)AddAddress:(UIButton *)sender {
    GFShippingAddressController *Add = [[GFShippingAddressController alloc]init];
    [self.navigationController pushViewController:Add animated:YES];
    
}

- (IBAction)payType:(UIButton *)sender {
    GFPayTypeController *Add = [[GFPayTypeController alloc]init];
    [self.navigationController pushViewController:Add animated:YES];
}

- (IBAction)subMit:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
