//
//  GFFriendsCityController.m
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFFriendsCityController.h"
#import "GFFindPeopleCell.h"
#import "GFSearchResultDController.h"

@interface GFFriendsCityController ()

@end

@implementation GFFriendsCityController
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)add
{
    //添加
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [FPTools setNavBar:self andTitle:@"同城好友"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(back) RightBtnWithImageFile:@"SJTJNavBtn" orRightTitle:nil andSEL:@selector(add)];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 8;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GFFindPeopleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[GFFindPeopleCell alloc]init];
    }
    [cell.stausBtn setTitle:@"加为好友" forState:UIControlStateNormal];
    [cell.stausView setImage:[UIImage imageNamed:@"AddFriend"]];
    [cell configCellSubViewWith:nil];
    // Configure the cell...
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GFSearchResultDController *searchRDC = [[GFSearchResultDController alloc]init];
    [self.navigationController pushViewController:searchRDC animated:YES];
}

@end
