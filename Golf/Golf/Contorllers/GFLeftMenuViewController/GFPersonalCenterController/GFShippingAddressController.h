//
//  GFShippingAddressController.h
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFShippingAddressController : UITableViewController
@property (strong, nonatomic) IBOutlet UIView *FootView;
- (IBAction)footBtnClike:(UIButton *)sender;

@end
