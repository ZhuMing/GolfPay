//
//  GFPersonalCenterController.m
//  Golf
//
//  Created by 朱明 on 15/2/4.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFPersonalCenterController.h"
#import "GFPersonalCenterHeaderView.h"
#import "GFFriendController.h"
#import "GFCollectController.h"
#import "GFMyOrderController.h"
#import "GFApplySettledController.h"
#import "GFFriendsCityController.h"
#import "GFMyGroupController.h"
#import "GFUserInfoModel.h"
#import "GFLoginViewController.h"



@interface GFPersonalCenterController ()
@property (nonatomic,strong) NSArray *dataList;
@end

@implementation GFPersonalCenterController
-(void)ShowleftMenu
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}
-(void)ShowRightMenu
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionRight animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [FPTools setNavBar:self andTitle:@"个人中心"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(ShowleftMenu) RightBtnWithImageFile:@"FPHome" orRightTitle:nil andSEL:@selector(ShowRightMenu)];
    GFPersonalCenterHeaderView *headerV = [[GFPersonalCenterHeaderView alloc]init];
    self.tableView.tableHeaderView = headerV;
    _dataList = @[@[@{@"leftImage":@"GFPersonalCenterQQHY",@"content":@"全球好友"},@{@"leftImage":@"GFPersonalCenterQQSC",@"content":@"全球收藏"},@{@"leftImage":@"GFPersonalCenterDDPJ",@"content":@"订单评价"}],@[@{@"leftImage":@"GFPersonalCenterSQRZ",@"content":@"申请入驻"},@{@"leftImage":@"GFPersonalCenterQZLT",@"content":@"群组聊天"},@{@"leftImage":@"GFPersonalCenterTCHY",@"content":@"同城好友"}]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *Arr = _dataList[section];
    return Arr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return 20;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuseIdentifier"];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSArray *Arr = _dataList[indexPath.section];
    NSDictionary *Dic = Arr[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:13];
    cell.textLabel.textColor = [UIColor darkTextColor];
    cell.imageView.image = [UIImage imageNamed:Dic[@"leftImage"]];
    cell.textLabel.text = Dic[@"content"];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section==0) {
        switch (indexPath.row) {
            case 0:
            {
                GFFriendController *friendC = [[GFFriendController alloc]init];
                [self.navigationController pushViewController:friendC animated:YES];
            }
                break;
            case 1:
            {
                
                
                if ([[GFUserInfoModel alloc]initWithLocal].Id.length>0) {
                    GFCollectController *friendC = [[GFCollectController alloc]init];
                    friendC.userModel = [[GFUserInfoModel alloc]initWithLocal];
                    [self.navigationController pushViewController:friendC animated:YES];
                }
                else
                {
                    GFLoginViewController *loginVC = [[GFLoginViewController alloc]init];
                    [loginVC setLoginSucess:^(GFUserInfoModel *userModel) {
                        GFCollectController *friendC = [[GFCollectController alloc]init];
                        friendC.userModel = userModel;
                        [self.navigationController pushViewController:friendC animated:YES];
                        
                    }];
                    [self presentViewController:loginVC animated:YES completion:nil];
                }
                
                
            }
                break;
            case 2:
            {
                GFMyOrderController *friendC = [[GFMyOrderController alloc]init];
                [self.navigationController pushViewController:friendC animated:YES];
            }
                break;
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row) {
            case 0:
            {
                GFApplySettledController *friendC = [[GFApplySettledController alloc]init];
                [self.navigationController pushViewController:friendC animated:YES];
            }
                break;
            case 1:
            {
                GFMyGroupController *myGroupC = [[GFMyGroupController alloc]init];
                [self.navigationController pushViewController:myGroupC animated:YES];
            }
                break;
            case 2:
            {
                GFFriendsCityController *friendC = [[GFFriendsCityController alloc]init];
                [self.navigationController pushViewController:friendC animated:YES];
            }
                break;
            default:
                break;
        }
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
