//
//  GFMyOrderController.m
//  Golf
//
//  Created by 朱明 on 15/2/13.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFMyOrderController.h"
#import "GFAllOrderCell.h"
#import "GFMyOrderCommentController.h"

#import "GFshoppingController.h"

typedef enum Htype
{
    ALL = 0,
    DPJ = 1,
    DFK = 2,
}OType;

@interface GFMyOrderController ()
{
    OType type;
}
@end

@implementation GFMyOrderController
-(void)ShowleftMenu
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)ShowRightMenu
{
    //add
}

- (void)viewDidLoad {
    [super viewDidLoad];
    type = ALL;
    [FPTools setNavBar:self andTitle:@"我的订单"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(ShowleftMenu) RightBtnWithImageFile:@"FPHome" orRightTitle:nil andSEL:@selector(ShowRightMenu)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return _HeaderView;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GFAllOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[GFAllOrderCell alloc]init];
    }
    [cell setCellAction:^(GFAllOrderCell *Ocell, UIButton *sender) {
        switch (sender.tag) {
            case 100:
            {
                GFMyOrderCommentController *moc = [[GFMyOrderCommentController alloc]init];
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:moc];
                
                [self presentViewController:nav animated:YES completion:nil];
                
            }
                break;
            case 101:
            {
                GFshoppingController *moc = [[GFshoppingController alloc]init];
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:moc];
                [self presentViewController:nav animated:YES completion:nil];
    
            }
                break;
            case 102:
            {
                
            }
                break;
            default:
                break;
        }
    }];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 97;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 66.0;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        //        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


- (IBAction)HeaderViewClike:(UIButton *)sender {
    switch (sender.tag) {
        case 1000:
        {
            type = ALL;
            [_AllBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_AllBtn setBackgroundColor:[UIColor colorWithRed:241.0/255.0 green:62.0/255.0 blue:76.0/255.0 alpha:1.0]];
        
            [_DPJBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_DPJBtn setBackgroundColor:[UIColor clearColor]];
            
            [_DFKBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_DFKBtn setBackgroundColor:[UIColor clearColor]];
        }
            break;
        case 1001:
        {
            type = DPJ;
            [_DPJBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_DPJBtn setBackgroundColor:[UIColor colorWithRed:241.0/255.0 green:62.0/255.0 blue:76.0/255.0 alpha:1.0]];
            
            [_AllBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_AllBtn setBackgroundColor:[UIColor clearColor]];
            
            [_DFKBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_DFKBtn setBackgroundColor:[UIColor clearColor]];
        }
            break;
        case 1002:
        {
            type = DFK;
            [_DFKBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_DFKBtn setBackgroundColor:[UIColor colorWithRed:241.0/255.0 green:62.0/255.0 blue:76.0/255.0 alpha:1.0]];
            
            [_AllBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_AllBtn setBackgroundColor:[UIColor clearColor]];
            
            [_DPJBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_DPJBtn setBackgroundColor:[UIColor clearColor]];
        }
            break;
        default:
            break;
    }
    [self.tableView reloadData];
}
@end
