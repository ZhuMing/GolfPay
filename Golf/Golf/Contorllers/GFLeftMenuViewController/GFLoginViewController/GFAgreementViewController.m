//
//  GFAgreementViewController.m
//  Golf
//
//  Created by 朱明 on 15/3/8.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFAgreementViewController.h"
#import "QCheckBox.h"

@interface GFAgreementViewController ()<QCheckBoxDelegate>
@end

@implementation GFAgreementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    QCheckBox *_check1 = [[QCheckBox alloc] initWithDelegate:self];
    _check1.frame = CGRectMake(23, CGRectGetMaxY(_contentLabel.frame)+10, 142, 30);
    [_check1 setTitle:@"同意高尔夫协议" forState:UIControlStateNormal];
    [_check1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_check1.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
    [self.view addSubview:_check1];
    [_check1 setChecked:NO];
    
    
    // Do any additional setup after loading the view from its nib.
}
#pragma mark - QCheckBoxDelegate

- (void)didSelectedCheckBox:(QCheckBox *)checkbox checked:(BOOL)checked {
    
    _nextBtn.enabled = checked;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)colse:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
