//
//  GFRegistrationViewController.h
//  Golf
//
//  Created by 朱明 on 15/1/30.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFRegistrationViewController : UIViewController
- (IBAction)dismiss:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberText;
@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UITextField *passWordText;
@property (weak, nonatomic) IBOutlet UIButton *selectCountryBtn;
- (IBAction)selectCountryAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *checkBoxIcon;
- (IBAction)AgreeAction:(UIButton *)sender;
- (IBAction)RegistAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *getCodeBtn;
- (IBAction)GetCodeAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *CountryTableView;


@end
