//
//  GFPerfectInformationController.h
//  Golf
//
//  Created by 朱明 on 15/2/5.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GFUserInfoModel.h"

@interface GFPerfectInformationController : UITableViewController

@property (nonatomic,strong)GFUserInfoModel *userModel;




@end
