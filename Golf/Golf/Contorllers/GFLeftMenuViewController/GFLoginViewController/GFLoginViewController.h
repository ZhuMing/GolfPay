//
//  GFLoginViewController.h
//  Golf
//
//  Created by 朱明 on 15/1/29.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GFUserInfoModel.h"

@interface GFLoginViewController : UIViewController
- (IBAction)dismiss:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *userText;
@property (weak, nonatomic) IBOutlet UITextField *passWordText;
@property (weak, nonatomic) IBOutlet UIImageView *CheckBoxImageView;


@property (nonatomic,copy) void (^loginSucess)(GFUserInfoModel*user);


- (IBAction)RememberPW:(UIButton *)sender;
- (IBAction)Login:(UIButton *)sender;
- (IBAction)Registration:(UIButton *)sender;
- (IBAction)TripartiteLogin:(UIButton *)sender;

@end
