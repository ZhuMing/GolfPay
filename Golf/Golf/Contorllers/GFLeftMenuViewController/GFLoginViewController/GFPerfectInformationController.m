//
//  GFPerfectInformationController.m
//  Golf
//
//  Created by 朱明 on 15/2/5.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFPerfectInformationController.h"
#import "GFPerfectInformationCell.h"
#import "GFPerfectInformationHeaderView.h"
#import "GFPerfectInformationFootCell.h"
#import "GFSelectTagView.h"

#import "FPUploadIMG.h"


@interface GFPerfectInformationController ()<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>
@property (nonatomic,strong)NSArray *dataList;
@property (nonatomic,strong)GFPerfectInformationHeaderView *headerView;

@property (nonatomic,strong) NSMutableDictionary *dict;


@end

@implementation GFPerfectInformationController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dict = [NSMutableDictionary dictionaryWithDictionary:@{@"Account":_userModel.Account}];
    __weak typeof(GFPerfectInformationController*) WeakSelf = self;
    _headerView = [[GFPerfectInformationHeaderView alloc]init];
    [_headerView setChangeIconClike:^{
        UIActionSheet *actionsheet = [[UIActionSheet alloc]initWithTitle:nil delegate:WeakSelf cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"相册",nil];
        actionsheet.tag = 1000;
        [actionsheet showInView:WeakSelf.view];
    }];
    self.tableView.tableHeaderView = _headerView;
     _dataList = @[@{@"leftImage":@"NC",@"content":@"昵称"},@{@"leftImage":@"XB",@"content":@"性别"},@{@"leftImage":@"NL",@"content":@"年龄"},@{@"leftImage":@"CJD",@"content":@"长居地"},@{@"leftImage":@"HBQ",@"content":@"个性标签"}];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==2)
    {
        return;
    }
    if (actionSheet.tag==1000)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        if (buttonIndex == 0)
        {
            if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                return;
            }
            [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        else if (buttonIndex == 1)
        {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        GFPerfectInformationCell *cell = (GFPerfectInformationCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        [_dict setObject:buttonIndex==0?@"1":@"0" forKey:@"Sex"];
        cell.subTitleLabel.text = buttonIndex==0?@"男":@"女";
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
//    FPUploadIMG *img = [[FPUploadIMG alloc]initWithImg:image andIsUd:YES];
    _headerView.userIconView.image = image;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return _dataList.count;
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==1) {
        GFPerfectInformationFootCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
        if (!cell) {
            cell = [[GFPerfectInformationFootCell alloc]init];
        }
        [cell setSaveInformation:^{
           //保存资料
            
            [FPApiRequst PostRequstWithUrl:Customer_InfoComplete andView:self andMSG:@"保存中..." andIMGName:@"Logo" andIsRealNameCertification:YES andParameters:_dict andImages:[NSMutableArray arrayWithArray:@[_headerView.userIconView.image]] Block:^(id objc, NSError *error) {
                if(!error)
                {
                    [FPApiRequst getRequstWithUrl:[NSString stringWithFormat:@"Customer/GetCustomerInfo/%@",_userModel.Account] andView:nil andMSG:nil andParameters:@{@"Account":_userModel.Account} Block:^(id objc, NSError *error) {
                        if (!error) {
                            GFUserInfoModel *model = [[GFUserInfoModel alloc]initWithDic:objc];
                        }
                    }];
                    [self showHint:@"保存资料成功！"];
                    [self dismissViewControllerAnimated:YES completion:^{
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"saveInfo" object:nil];
                    }];
                }
            }];
        }];
        [cell setNextStep:^{
            //跳过
            [self dismissViewControllerAnimated:YES completion:^{
                [[NSNotificationCenter defaultCenter]postNotificationName:@"saveInfo" object:nil];
            }];
        }];
        return cell;
    }
    
    GFPerfectInformationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[GFPerfectInformationCell alloc]init];
    }
    NSDictionary *Dic = _dataList[indexPath.row];
    cell.titleLabel.text = Dic[@"content"];
    cell.iconView.image = [UIImage imageNamed:Dic[@"leftImage"]];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1) {
        return 59;
    }
    return 44.0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        {
            UIAlertView *Alet = [[UIAlertView alloc]initWithTitle:@"昵称" message:nil delegate:self cancelButtonTitle:@"取消"otherButtonTitles:@"确定", nil];
            Alet.alertViewStyle = UIAlertViewStylePlainTextInput;
            Alet.tag = 1000;
            [Alet show];
        }
            break;
        case 1:
        {
            UIActionSheet *actionsheet = [[UIActionSheet alloc]initWithTitle:@"请选择性别" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"男", @"女",nil];
            [actionsheet showInView:self.view];
        }
            break;
        case 2:
        {
            //年龄
            UIAlertView *Alet = [[UIAlertView alloc]initWithTitle:@"年龄" message:nil delegate:self cancelButtonTitle:@"取消"otherButtonTitles:@"确定", nil];
            Alet.alertViewStyle = UIAlertViewStylePlainTextInput;
            [Alet show];
        }
            break;
        case 3:
        {
            //地区
            GFSelectTagView *selectTagView = [[GFSelectTagView alloc]init];
            selectTagView.isArea = YES;
            [selectTagView setDismiss:^{
                [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomBottom];
            }];
            [selectTagView setOK:^(GFTagModel *model) {
                
                GFPerfectInformationCell *cell = (GFPerfectInformationCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
                cell.subTitleLabel.text = model.TagName;
                [_dict setObject:model.Id forKey:@"AreaTagId"];
                [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomBottom];
            }];
            [self presentPopupViewController:selectTagView animationType:MJPopupViewAnimationSlideBottomBottom];
        }
            break;
        case 4:
        {
            //个性表签
            GFSelectTagView *selectTagView = [[GFSelectTagView alloc]init];
            [selectTagView setDismiss:^{
                [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomBottom];
            }];
            [selectTagView setOK:^(GFTagModel *model) {
                GFPerfectInformationCell *cell = (GFPerfectInformationCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
                cell.subTitleLabel.text = model.TagName;
                [_dict setObject:model.Id forKey:@"IndividualityId"];
                [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomBottom];
            }];
            [self presentPopupViewController:selectTagView animationType:MJPopupViewAnimationSlideBottomBottom];
        }
            break;
        default:
            break;
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1000) {
        if(buttonIndex==1)
        {
            UITextField *nickNameText=[alertView textFieldAtIndex:0];
            [nickNameText resignFirstResponder];
            //设置昵称
            if(nickNameText.text.length<=0)
            {
                [self showHint:@"请输入昵称！"];
                return;
            }
            GFPerfectInformationCell *cell = (GFPerfectInformationCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            cell.subTitleLabel.text = nickNameText.text;
            [_dict setObject:nickNameText.text forKey:@"NikeName"];
        }
    }
    else
    {
        if(buttonIndex==1)
        {
            UITextField *ageText=[alertView textFieldAtIndex:0];
            [ageText resignFirstResponder];
            //设置年龄
            if(ageText.text.length<=0)
            {
                [self showHint:@"请输入年龄！"];
                return;
            }
            GFPerfectInformationCell *cell = (GFPerfectInformationCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
            cell.subTitleLabel.text = ageText.text;
            [_dict setObject:ageText.text forKey:@"Age"];
        }
    }
}


@end
