//
//  GFRegistrationViewController.m
//  Golf
//
//  Created by 朱明 on 15/1/30.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFRegistrationViewController.h"
#import "GFPerfectInformationController.h"
#import "GFAgreementViewController.h"

#import "GFTagModel.h"
#import "GFUserInfoModel.h"

@interface GFRegistrationViewController ()
{
    NSTimer *timer;
    int sec;
    NSMutableArray *CountryArr;
    GFTagModel *country;
}
@property (nonatomic,strong) NSMutableDictionary *Parameters;
@end

@implementation GFRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadDataArea];
    self.Parameters = [NSMutableDictionary new];
    [self.navigationController setNavigationBarHidden:YES];
    [_phoneNumberText addTarget:self action:@selector(EditPhoneNumber:) forControlEvents:UIControlEventEditingChanged];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)dismiss:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)loadDataArea
{
    
    [FPApiRequst getRequstWithUrl:@"Merchants/GetCountry" andView:nil andMSG:nil andParameters:nil Block:^(id objc, NSError *error) {
        if (!error) {
            CountryArr = [NSMutableArray array];
            for (NSDictionary *dic in objc) {
                GFTagModel *model = [[GFTagModel alloc]initWithDic:dic];
                [CountryArr addObject:model];
            }
            if(CountryArr.count>0)
            {
//                [_areaBtn setTitle:((GFTagModel*)_AreaArr[0]).TagName forState:UIControlStateNormal];
//                [self.Parameters setObject:((GFTagModel*)_AreaArr[0]).Id forKey:@"AreaId"];
            }
            
            [_CountryTableView reloadData];
        }
    }];
}




- (IBAction)selectCountryAction:(UIButton *)sender
{
    //选择国家
    [_CountryTableView setHidden:NO];
}
- (IBAction)AgreeAction:(UIButton *)sender {
    //同意协议
    
    [self presentViewController:[[GFAgreementViewController alloc]init] animated:YES completion:nil];
}

- (IBAction)RegistAction:(UIButton *)sender {
    //注册
    if (![FPTools isMobileNumber:_phoneNumberText.text]) {
        [self showHint:@"请输入正确的手机号码！"];
        return;
    }
    if (_passWordText.text.length<6)
    {
        [self showHint:@"密码长度要大于6位！"];
        return;
    }
    if(_codeText.text.length<=0)
    {
        [self showHint:@"请输入验证码！"];
        return;
    }
    if (!country) {
        [self showHint:@"请输入验证码！"];
        return;
    }
    /*GFPerfectInformationController *informationC = [[GFPerfectInformationController alloc]init];
    [self.navigationController pushViewController:informationC animated:YES];*/
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"Account":_phoneNumberText.text,@"Password":_passWordText.text,@"Code":_codeText.text,@"Country":country.Id}];
    
    [FPApiRequst PostRequstWithUrl:Customer_Customer_Add andView:self andMSG:@"注册中..." andParameters:dic Block:^(id objc, NSError *error) {
        
        
        if (!error) {
            if (![objc[@"Message"]isEqualToString:@"OK"])
            {
                [self showHint:objc[@"Message"]];
                return ;
            }
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"Account":_phoneNumberText.text,@"Password":_passWordText.text}];
            [FPApiRequst PostRequstWithUrl:Customer_Customer_Login andView:nil andMSG:nil andParameters:dic Block:^(id objc, NSError *error) {
                if (!error&&!objc[@"Message"])
                {
                    if (objc[@"Message"])
                    {
                        [self showHint:objc[@"Message"]];
                        return ;
                    }
                    GFUserInfoModel *userModel = [[GFUserInfoModel alloc]initWithDic:objc];
                    GFPerfectInformationController *informationC = [[GFPerfectInformationController alloc]init];
                    informationC.userModel = userModel;
                    [self.navigationController pushViewController:informationC animated:YES];
                }
            }];
            
     
        }
    }];
    
    
   
}
-(void)Timing:(NSTimer*)tm
{
    sec--;
    NSString *str = nil;
    if (sec==0) {
        [timer invalidate];
        _getCodeBtn.enabled = YES;
        str = @"获取验证码";
    }
    else
    {
        str = [NSString stringWithFormat:@"%i秒后重试",sec];
    }
    [_getCodeBtn setTitle:str forState:UIControlStateNormal];
}

- (IBAction)GetCodeAction:(UIButton *)sender {
    //获取验证码
    sec = 30;
    [timer invalidate];
    _getCodeBtn.enabled = NO;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(Timing:) userInfo:nil repeats:YES];
    [FPApiRequst getRequstWithUrl:Get_CodeNumber(_phoneNumberText.text) andView:self andMSG:@"验证码发送中..." andParameters:nil Block:^(id objc, NSError *error) {
        if (!error) {
            [self showHint:@"发送成功！"];
        }
    }];
}
- (void)EditPhoneNumber:(UITextField *)sender
{
    if ([FPTools isMobileNumber:sender.text]) {
        _getCodeBtn.enabled = YES;
    }
    else
    {
        _getCodeBtn.enabled = NO;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return CountryArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuseIdentifier"];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    
    cell.textLabel.text = ((GFTagModel*)CountryArr[indexPath.row]).CountryName;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [_selectCountryBtn setTitle:((GFTagModel*)CountryArr[indexPath.row]).CountryName forState:UIControlStateNormal] ;
//    [self.Parameters setObject:((GFTagModel*)CountryArr[indexPath.row]).Id forKey:@"BehaviorId"];
    country = ((GFTagModel*)CountryArr[indexPath.row]);
    [_CountryTableView setHidden:YES];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 20;
}

@end
