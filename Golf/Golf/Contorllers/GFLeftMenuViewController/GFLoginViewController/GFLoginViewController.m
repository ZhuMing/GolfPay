//
//  GFLoginViewController.m
//  Golf
//
//  Created by 朱明 on 15/1/29.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFLoginViewController.h"
#import "GFRegistrationViewController.h"
#import "GFUserInfoModel.h"

@interface GFLoginViewController ()

@end

@implementation GFLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismiss:) name:@"saveInfo" object:nil];
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"saveInfo" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dismiss:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)RememberPW:(UIButton *)sender {
    //记住密码
    
}

- (IBAction)Login:(UIButton *)sender {
    //登录
    if(_userText.text.length<=0)
    {
        [self showHint:@"请输入用户名！"];
        return;
    }
    if(_passWordText.text.length<=0)
    {
        [self showHint:@"请输入密码！"];
        return;
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"Account":_userText.text,@"Password":_passWordText.text}];

    [FPApiRequst PostRequstWithUrl:Customer_Customer_Login andView:self andMSG:@"登录中..." andParameters:dic Block:^(id objc, NSError *error) {
        if (!error)
        {
            GFUserInfoModel *userModel = [[GFUserInfoModel alloc]initWithDic:objc];
            if (_loginSucess) {
                _loginSucess(userModel);
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }];
}

- (IBAction)Registration:(UIButton *)sender {
    //注册
    GFRegistrationViewController *registVC =[[GFRegistrationViewController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:registVC];
    registVC.modalTransitionStyle = 1;
    [self presentViewController:nav animated:YES completion:nil];
}

- (IBAction)TripartiteLogin:(UIButton *)sender {
    //三方登录
    
}
@end
