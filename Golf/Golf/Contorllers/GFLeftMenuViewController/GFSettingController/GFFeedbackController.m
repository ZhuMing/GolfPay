//
//  GFFeedbackController.m
//  Golf
//
//  Created by 朱明 on 15/2/7.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFFeedbackController.h"

@interface GFFeedbackController ()
@property (nonatomic,strong) NSMutableArray *dataList;
@end

@implementation GFFeedbackController
- (void)configSubView
{
    _contentView.layer.masksToBounds = YES;
    _contentView.layer.cornerRadius = 5.0;
    _contentView.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:220/255.0].CGColor;
    _contentView.layer.borderWidth = 1.0;
    
    _selectBtn.layer.masksToBounds = YES;
    _selectBtn.layer.cornerRadius = 5.0;
    _selectBtn.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:220/255.0].CGColor;
    _selectBtn.layer.borderWidth = 1.0;
    
}
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [FPTools setNavBar:self andTitle:@"意见反馈"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(back) RightBtnWithImageFile:@"" orRightTitle:nil andSEL:nil];
    _dataList = @[@"1、经常闪退",@"2、明显卡顿",@"3、无法搜索"];
    [self configSubView];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _dataList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuseIdentifier"];
    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    cell.textLabel.text = _dataList[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_selectBtn setTitle:_dataList[indexPath.row] forState:UIControlStateNormal];
    [_feedBackTypeTableView setHidden:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)selectAction:(UIButton *)sender {
    [_feedBackTypeTableView setHidden:NO];
}
- (IBAction)subMit:(UIButton *)sender {
    
}
@end
