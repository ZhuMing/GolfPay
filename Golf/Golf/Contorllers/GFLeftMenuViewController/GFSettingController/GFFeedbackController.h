//
//  GFFeedbackController.h
//  Golf
//
//  Created by 朱明 on 15/2/7.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFFeedbackController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
- (IBAction)selectAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *feedBackTypeTableView;
@property (weak, nonatomic) IBOutlet UITextView *contentView;
- (IBAction)subMit:(UIButton *)sender;

@end
