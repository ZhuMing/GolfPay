//
//  GFSettingController.h
//  Golf
//
//  Created by 朱明 on 15/2/7.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GFSettingController : UITableViewController
@property (strong, nonatomic) IBOutlet UIView *footView;
- (IBAction)logout:(UIButton *)sender;

@end
