//
//  GFProductDetailsController.h
//  Golf
//
//  Created by 朱明 on 15/3/12.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TP100ProductModel.h"

@interface GFProductDetailsController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *SPImageView;
@property (weak, nonatomic) IBOutlet UILabel *SPNameLbael;
@property (weak, nonatomic) IBOutlet UIImageView *VIpIcon;
@property (weak, nonatomic) IBOutlet UILabel *JGLabel;
- (IBAction)CollectAction:(id)sender;
- (IBAction)PurchaseAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic)TP100ProductModel*model;



@end
