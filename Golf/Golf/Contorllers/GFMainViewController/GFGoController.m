//
//  GFGoController.m
//  Golf
//
//  Created by 朱明 on 15/2/10.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFGoController.h"
#import "GFFriendCell.h"
#import "GFUserInfoModel.h"

@interface GFGoController ()

@end

@implementation GFGoController
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)add
{
    //添加
    
}
-(void)loadData:(MJRefreshBaseView *)refreshView
{
    NSString *urlStr = _isGo?@"Want":@"BeenList";
    [FPApiRequst getRequstWithUrl:urlStr andView:self andMSG:@"加载中..." andParameters:nil Block:^(id objc, NSError *error) {
        
        if (!error) {
            
            self.dateList = [NSMutableArray array];
            for (NSDictionary *item in (NSArray*)objc) {
                
                [self.dateList addObject:[[GFUserInfoModel alloc]initWithDictionary:item error:nil]];
            }
             [self doneWithView:refreshView];
        }
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _isGo?[self clikeGO]:[self clikeBeen];
    [FPTools setNavBar:self andTitle:@"数据统计"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(back) RightBtnWithImageFile:@"SJTJNavBtn" orRightTitle:nil andSEL:@selector(add)];
    self.tableView.tableHeaderView = _HeaderView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dateList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GFFriendCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[GFFriendCell alloc]init];
    }
    [cell configCellSubViewWith:self.dateList[indexPath.row]];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)clikeGO
{
    [_goBtn setTitleColor:[UIColor colorWithRed:241.0/255.0 green:62.0/255.0 blue:76.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_goLine setHidden:NO];
    [_BeenBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_beenLine setHidden:YES];
    
    _isGo = YES;
    
}
-(void)clikeBeen
{
    [_goBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_goLine setHidden:YES];
    [_BeenBtn setTitleColor:[UIColor colorWithRed:241.0/255.0 green:62.0/255.0 blue:76.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_beenLine setHidden:NO];
    _isGo = NO;
}
- (IBAction)changeStaus:(UIButton *)sender {
    switch (sender.tag) {
        case 10:
        {
            //想去
            [self clikeGO];
        }
            break;
        case 11:
        {
            //去过
            [self clikeBeen];
        }
            break;
        default:
            break;
    }
    [self loadData:self.refreshView];
}
@end
