//
//  GFaddCommentController.m
//  Golf
//
//  Created by 朱明 on 15/2/7.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFaddCommentController.h"
#import "GFAddCommentImgCell.h"
#import "FPUploadIMG.h"
#import "GFUserInfoModel.h"
@interface GFaddCommentController ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSInteger imgcount;
}
@property (nonatomic,strong) NSMutableArray *images;
@property (nonatomic,strong)NSMutableDictionary *parameters;

@end

@implementation GFaddCommentController
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [FPTools setNavBar:self andTitle:@"意见评价"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(back) RightBtnWithImageFile:@"" orRightTitle:nil andSEL:nil];
    _userIconView.layer.masksToBounds = YES;
    _userIconView.layer.cornerRadius = _userIconView.frame.size.width/2;
    imgcount = 0;
    _images = [NSMutableArray arrayWithArray:@[[[FPUploadIMG alloc]initWithImg:[UIImage imageNamed:@"addImg"] andIsUd:NO]]];
    
    [self.CollectionView registerClass:[GFAddCommentImgCell class] forCellWithReuseIdentifier:@"GFAddCommentImgCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _images.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GFAddCommentImgCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GFAddCommentImgCell" forIndexPath:indexPath];
    cell.imgView.image = ((FPUploadIMG*)_images[indexPath.row]).img;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIActionSheet *actionsheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"相册",nil];
    actionsheet.tag = indexPath.row;
    [actionsheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==2) {
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.view.tag = actionSheet.tag;
    if (buttonIndex == 0)
    {
        if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            [self showHint:@"该设备不支持拍照"];
            return;
        }
        [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else if (buttonIndex == 1)
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    FPUploadIMG *img = [[FPUploadIMG alloc]initWithImg:image andIsUd:YES];
    if(((FPUploadIMG*)_images[picker.view.tag]).isUpLoad)
    {
        [_images removeObjectAtIndex:picker.view.tag];
        [_images insertObject:img atIndex:picker.view.tag];
    }
    else
    {
        imgcount++;
        if (imgcount>=3) {
            [_images removeLastObject];
            [_images addObject:img];
        }
        else
            [_images insertObject:img atIndex:_images.count-1];
    }
    [self.CollectionView reloadData];
    [picker dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submit:(UIButton *)sender {
    
    _parameters = [NSMutableDictionary new];
    
    
    
    [_parameters setObject:self.activID forKey:@"ActivityId"];
    [_parameters setObject:[[GFUserInfoModel alloc]initWithLocal].Id forKey:@"CustomerID"];
    [_parameters setObject:self.contentView.text forKey:@"Review"];
//    [_parameters setObject:self.images forKey:@"File"];
    
    FPUploadIMG *img = _images[0];
    
    [FPApiRequst PostRequstWithUrl:@"Activities/ActivityReviews"  andView:self andMSG:@"上传中..." andIMGName:@"File" andIsRealNameCertification:YES andParameters:_parameters andImages:@[img.img] Block:^(id objc, NSError *error) {
        if (!error) {
            [self showHint:@"评论成功！"];
        }
        
    }];
    
    
    
}
@end
