//
//  GFMainViewController.h
//  Golf
//
//  Created by 朱明 on 15/1/28.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFBaseController.h"
typedef enum Type
{
    Top100 = 0,
    LookForAJob = 1,
    Trades =2
}mainVCType;

@interface GFMainViewController : GFBaseController

@property (nonatomic,assign)mainVCType type;

@property (strong, nonatomic) IBOutlet UIButton *RightMenuBtn;
- (IBAction)showRightMenu:(UIButton *)sender;

@end
