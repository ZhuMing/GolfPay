//
//  GFActivityDetailViewController.h
//  Golf
//
//  Created by 朱明 on 15/1/31.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GFActivityModel.h"


@interface GFActivityDetailViewController : UITableViewController
@property (nonatomic,strong) GFActivityModel *model;
@property (strong, nonatomic) IBOutlet UIView *BottomView;

- (IBAction)commentAction:(UIButton *)sender;

@end
