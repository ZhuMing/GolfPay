//
//  GFGoController.h
//  Golf
//
//  Created by 朱明 on 15/2/10.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFBaseController.h"

@interface GFGoController : GFBaseController

@property (nonatomic,assign) BOOL isGo;

@property (weak, nonatomic) IBOutlet UIButton *goBtn;


- (IBAction)changeStaus:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *beenLine;
@property (weak, nonatomic) IBOutlet UIButton *BeenBtn;
@property (weak, nonatomic) IBOutlet UIView *goLine;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIView *HeaderView;


@end
