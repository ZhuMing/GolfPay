//
//  GFProductDetailsController.m
//  Golf
//
//  Created by 朱明 on 15/3/12.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFProductDetailsController.h"
#import "GFProductDetailsCell.h"
#import "GFReviewCell.h"

@interface GFProductDetailsController ()

@end

@implementation GFProductDetailsController
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)add
{
    //添加
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [FPTools setNavBar:self andTitle:@"商品详情"];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"FPBack" orTitle:nil andSEL:@selector(back) RightBtnWithImageFile:@"SJTJNavBtn" orRightTitle:nil andSEL:@selector(add)];
    
    
    self.tableView.tableHeaderView = self.headerView;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)CollectAction:(id)sender {
}

- (IBAction)PurchaseAction:(id)sender {
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0) {
        GFProductDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
        if (!cell) {
            cell = [[GFProductDetailsCell alloc]init];
        }
        
        return cell;
    }
    
    GFReviewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[GFReviewCell alloc]init];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.section==0?276:114;
}
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return section==0?@"商品详情":@"用户评论";
}




@end
