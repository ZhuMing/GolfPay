//
//  GFActivityDetailViewController.m
//  Golf
//
//  Created by 朱明 on 15/1/31.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFActivityDetailViewController.h"
#import "GFaddCommentController.h"
#import "GFGoController.h"
#import "GFLoginViewController.h"
#import "GFProductDetailsController.h"



#import "GFMatchingServiceCell.h"
#import "GFActivityDetailHeaderView.h"
#import "GFActivityInfoCell.h"
#import "GFMatchingServiceCell.h"
#import "GFReviewCell.h"
#import "GFReviewSecionView.h"

#import "TP100ActivityImageModel.h"
#import "TP100ProductModel.h"
#import "TP100productReviewModel.h"
#import "GFUserInfoModel.h"



@interface GFActivityDetailViewController ()

@property (nonatomic,strong)GFActivityDetailHeaderView *headerView;
@property (nonatomic,strong)NSMutableArray *matchServiceDataSource;
@property (nonatomic,strong)NSMutableArray *activPhotos;
@property (nonatomic,strong)NSMutableArray *activReview;
@property (nonatomic,strong)NSString *headTitle;
@property (nonatomic,strong)NSString *headArea;
@property (nonatomic,strong)NSString *headDateTime;
@property (nonatomic,strong)NSString *ATagName;
@property (nonatomic,strong)NSString *BTagName;
@property (nonatomic,strong)NSString *actContent;
@property (nonatomic,strong)NSMutableDictionary *parameters;

@end

@implementation GFActivityDetailViewController

-(void)loadData
{
    
    [FPApiRequst getRequstWithUrl:[NSString stringWithFormat:@"Activities/GetActivityDetail/%@",_model.Id] andView:nil andMSG:nil andParameters:nil Block:^(id objc, NSError *error) {
        if (!error)
        {
            
            NSLog(@"objc === %@ === %@ === %@",objc,objc[@"Effective"],[objc StringForKey:@"ProductContent"]);
            
            self.headTitle = [objc StringForKey:@"ATagName"];
            self.headArea = objc[@"Address"];
            self.headDateTime = objc[@"Effective"];
            self.ATagName = [objc StringForKey:@"ATagName"];
            self.BTagName = [objc StringForKey:@"BTagName"];
            
            self.activPhotos = [NSMutableArray new];
            for (NSDictionary *item in objc[@"activityImage"]) {
                [self.activPhotos addObject:[[TP100ActivityImageModel alloc]initWithDic:item].ImageUrl];
            }
            
            self.actContent = objc[@"activityContent"];
            self.matchServiceDataSource = [NSMutableArray new];
            for (NSDictionary *item in objc[@"product"]) {
                [self.matchServiceDataSource addObject:[[TP100ProductModel alloc]initWithDic:item]];
            }
            
            NSLog(@"self.matchServiceDataSource.count = %lu",(unsigned long)self.matchServiceDataSource.count);
            
            self.activReview = [NSMutableArray new];
            for (NSDictionary *item in objc[@"productReview"]) {
                [self.activReview addObject:[[TP100productReviewModel alloc]initWithDic:item]];
            }

            [self loadheadData];
            [self.tableView reloadData];
        }
        
    }];
    
}


-(void)loadheadData{

    _headerView.titleLabel.text = self.headTitle;
    _headerView.AreaLabel.text = self.headArea;
    _headerView.dateTimeLabel.text = self.headDateTime;
    _headerView.tagLabel.text = [NSString stringWithFormat:@"        %@、%@",self.ATagName,self.BTagName];
    
    [_headerView configViewWithArr:self.activPhotos];
    [_headerView.CollectionView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    
    _headerView = [[GFActivityDetailHeaderView alloc]init];
    
    [_headerView setClikeNavBtn:^(UIButton *sender)
    {
        switch (sender.tag) {
            case 100:
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
                break;
            case 101:
            {
                GFUserInfoModel *user = [[GFUserInfoModel alloc]initWithLocal];
                if (user.Id.length<=0) {
                    GFLoginViewController *loginVC = [[GFLoginViewController alloc]init];
                    
                    [loginVC setLoginSucess:^(GFUserInfoModel *userModel) {
                      NSMutableDictionary  *parameters = [NSMutableDictionary new];
                        [parameters setObject:_model.Id forKey:@"ItemId"];
                        [parameters setObject:userModel.Id forKey:@"CustomerId"];
                        [parameters setObject:_model.ITagName forKey:@"ItemTitle"];
                        
                        [FPApiRequst PostRequstWithUrl:@"Customer/AddFavoritesActivity" andView:self andMSG:@"收藏中..." andParameters:parameters Block:^(id objc, NSError *error) {
                            if (!error) {
                                [self showHint:@"收藏成功！"];
                            }
                        }];
                        
                    }];
                    [self presentViewController:loginVC animated:YES completion:nil];
                    return;
                    
                }
                NSMutableDictionary  *parameters = [NSMutableDictionary new];
                [parameters setObject:_model.Id forKey:@"ItemId"];
                [parameters setObject:user.Id forKey:@"CustomerId"];
                [parameters setObject:_model.ITagName forKey:@"ItemTitle"];
                
                [FPApiRequst PostRequstWithUrl:@"Customer/AddFavoritesActivity" andView:self andMSG:@"收藏中..." andParameters:parameters Block:^(id objc, NSError *error) {
                    if (!error) {
                        [self showHint:@"收藏成功！"];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }];
    self.tableView.tableHeaderView = _headerView;
    self.tableView.tableFooterView = _BottomView;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    if (section==2) {
        
        return [self.activReview count];
        
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    switch (indexPath.section) {
        case 0:
        {
            if (!cell) {
                cell = [[GFActivityInfoCell alloc]init];
            }
            
        ((GFActivityInfoCell *)cell).contentLabel.text = self.actContent;
            return cell;
            
        }
            break;
        case 1:
        {
            if (!cell) {
                cell = [[GFMatchingServiceCell alloc]init];
            }
            
        ((GFMatchingServiceCell *)cell).dataSource = self.matchServiceDataSource;
        [((GFMatchingServiceCell *)cell).tableView reloadData];
            
            
            //查看商品详情
            [((GFMatchingServiceCell*)cell) setSelectIndexCell:^(GFMatchingServiceCell *msCell,TP100ProductModel*model) {
                GFProductDetailsController *pdc = [[GFProductDetailsController alloc]init];
                pdc.model = model;
                [self.navigationController pushViewController:pdc animated:YES];
            }];
            
            
            //想去
            [((GFMatchingServiceCell*)cell) setFootGo:^{
                [self wannaGo];
            }];
            //去过
            [((GFMatchingServiceCell*)cell) setFootBeen:^{
                [self going];
            }];
            
            //去过列表
            [((GFMatchingServiceCell*)cell) setFootGoList:^{
                
                GFGoController *goC = [[GFGoController alloc]init];
                [self.navigationController pushViewController:goC animated:YES];
            }];
            
            //想去列表
            [((GFMatchingServiceCell*)cell) setFootBeenList:^{
                GFGoController *goC = [[GFGoController alloc]init];
                 goC.isGo = YES;
                [self.navigationController pushViewController:goC animated:YES];
            }];
        
            //更多
            [((GFMatchingServiceCell*)cell) setFootMore:^{
                
            }];
            return cell;
        }
            break;
        case 2:
        {
            if (!cell) {
                cell = [[GFReviewCell alloc]init];
            }
            
            TP100productReviewModel *model = self.activReview[indexPath.row];
            ((GFReviewCell *)cell).contentLabel.text = model.Review;
            return cell;
        }
            break;
        default:
            break;
    }
    // Configure the cell...
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            return 136.0;
            
        }
            break;
        case 1:
        {
            
            return 104+56*self.matchServiceDataSource.count;
        }
            break;
        case 2:
        {
            return 114;
        }
            break;
        default:
            break;
    }
    return 50;
}




-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==2) {
        return  [[GFReviewSecionView alloc]init];
    }
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==2) {
        return  55;
    }
    return 0;
}
-(void)going
{
    GFUserInfoModel *user = [[GFUserInfoModel alloc]initWithLocal];
    if (user.Id.length<=0) {
        GFLoginViewController *loginVC = [[GFLoginViewController alloc]init];
        [loginVC setLoginSucess:^(GFUserInfoModel *userModel) {
            NSMutableDictionary *parameters = [NSMutableDictionary new];
            [parameters setObject:_model.Id forKey:@"ActivityId"];
            [parameters setObject:userModel.Id forKey:@"CustomerId"];
            [FPApiRequst PostRequstWithUrl:[NSString stringWithFormat:@"Been"] andView:nil andMSG:nil andParameters:parameters Block:^(id objc, NSError *error) {
                if (!error)
                {
                    
                }
            }];
            
        }];
        [self presentViewController:loginVC animated:YES completion:nil];
        return;
        
    }
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    [parameters setObject:_model.Id forKey:@"ActivityId"];
    [parameters setObject:user.Id forKey:@"CustomerId"];
    [FPApiRequst PostRequstWithUrl:[NSString stringWithFormat:@"Been"] andView:nil andMSG:nil andParameters:parameters Block:^(id objc, NSError *error) {
        if (!error)
        {
            
        }
    }];
}

-(void)wannaGo{

    GFUserInfoModel *user = [[GFUserInfoModel alloc]initWithLocal];
    if (user.Id.length<=0) {
        GFLoginViewController *loginVC = [[GFLoginViewController alloc]init];
        [loginVC setLoginSucess:^(GFUserInfoModel *userModel) {
           NSMutableDictionary *parameters = [NSMutableDictionary new];
            [parameters setObject:_model.Id forKey:@"ActivityId"];
            [parameters setObject:userModel.Id forKey:@"CustomerId"];
            [FPApiRequst PostRequstWithUrl:[NSString stringWithFormat:@"WantToGo"] andView:nil andMSG:nil andParameters:parameters Block:^(id objc, NSError *error) {
                if (!error)
                {
                    
                }
            }];
            
        }];
        [self presentViewController:loginVC animated:YES completion:nil];
        return;

    }
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    [parameters setObject:_model.Id forKey:@"ActivityId"];
    [parameters setObject:user.Id forKey:@"CustomerId"];
    [FPApiRequst PostRequstWithUrl:[NSString stringWithFormat:@"WantToGo"] andView:nil andMSG:nil andParameters:parameters Block:^(id objc, NSError *error) {
        if (!error)
        {
            
        }
    }];
}

- (IBAction)commentAction:(UIButton *)sender {
    
    GFUserInfoModel *user = [[GFUserInfoModel alloc]initWithLocal];
    if (user.Id.length<=0) {
        GFLoginViewController *loginVC = [[GFLoginViewController alloc]init];
        [loginVC setLoginSucess:^(GFUserInfoModel *userModel) {
            _parameters = [NSMutableDictionary new];
            [_parameters setObject:_model.Id forKey:@"ActivityId"];
            [_parameters setObject:userModel.Id forKey:@"CustomerId"];
            [_parameters setObject:@(1) forKey:@"ReservationTotal"];
            
            [FPApiRequst PostRequstWithUrl:[NSString stringWithFormat:@"OpinionEvaluate-Add"] andView:nil andMSG:nil andParameters:_parameters Block:^(id objc, NSError *error) {
                if (!error)
                {
                    GFaddCommentController *addComment = [[GFaddCommentController alloc]init];
                    addComment.activID = _model.Id;
                    [self.navigationController pushViewController:addComment animated:YES];
                }
            }];
        }];
        [self presentViewController:loginVC animated:YES completion:nil];
        return;
    }
    
    
    GFaddCommentController *addComment = [[GFaddCommentController alloc]init];
    addComment.activID = _model.Id;
    [self.navigationController pushViewController:addComment animated:YES];
    
    
}
@end
