//
//  GFMainViewController.m
//  Golf
//
//  Created by 朱明 on 15/1/28.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "GFMainViewController.h"
#import "GFActivityDetailViewController.h"
#import "DOPDropDownMenu.h"

#import "GFMainCell.h"
#import "GFCategoriesModel.h"
#import "GFActivityModel.h"
#import "RWDropdownMenu.h"

@interface GFMainViewController ()<DOPDropDownMenuDataSource, DOPDropDownMenuDelegate>
@property (nonatomic,strong) NSMutableArray *Categories;
@property (nonatomic,strong)NSMutableDictionary *parameters;
@property (nonatomic,strong) DOPDropDownMenu *menu;
@end

@implementation GFMainViewController

-(void)loadDataCategories
{
    
    switch (self.type) {
        case Top100:
        {
            [FPApiRequst getRequstWithUrl:Activities_GetCategories andView:nil andMSG:nil andParameters:nil Block:^(id objc, NSError *error) {
                if (!error)
                {
                    [_Categories removeAllObjects];
                    NSMutableArray *Arr = [NSMutableArray array];
                    for (NSDictionary *item in objc) {
                        [Arr addObject: [[GFCategoriesModel alloc]initWithDic:item]];
                    }
                    if(Arr.count>0)
                    {
                        [_Categories addObject:Arr];

                    }
                    [self configNavMenuBtn];
                }
            }];
        }
            break;
        case LookForAJob:
        {
            _Categories = @[@[@"中国境内",@"中国境外"],@[@"上海",@"北京",@"深圳",@"香港"],@[@"学Golf",@"买球具",@"定场",@"参加球赛",@"参加聚会",@"买礼物"]];
            [self configNavMenuBtn];
        }
            break;
        case Trades:
        {
            _Categories = @[@[@"上海",@"北京",@"深圳",@"香港"],@[@"Golf",@"spa",@"traveling"],@[@"Golf",@"spa",@"traveling"]];
            [self configNavMenuBtn];
        }
            break;
        default:
            break;
    }
    
    
}
- (void)presentMenu
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}
-(void)presentRMenu
{
     [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionRight withOffset:200 animated:YES];
}
- (void)configNavMenuBtn
{
    _menu = [[DOPDropDownMenu alloc] initWithOrigin:CGPointMake(0, 0) andHeight:40];
    _menu.dataSource = self;
    _menu.delegate = self;
}
- (NSInteger)numberOfColumnsInMenu:(DOPDropDownMenu *)menu {
    return self.Categories.count;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column {
    NSArray *Arr = self.Categories[column];
    return Arr.count;
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForRowAtIndexPath:(DOPIndexPath *)indexPath {
    
    NSArray *Arr = self.Categories[indexPath.column];
    if([Arr[indexPath.row] isKindOfClass:[GFCategoriesModel class]])
    {
        GFCategoriesModel *model = Arr[indexPath.row];
        return model.TypeName;
    }
    else
    {
        return Arr[indexPath.row];
    }
    
    
    /*switch (indexPath.column) {
        case 0:
        {
            
        }
            break;
        case 1: return self.genders[indexPath.row];
            break;
        case 2: return self.ages[indexPath.row];
            break;        default:
            return nil;
            break;
    }*/

}

- (void)menu:(DOPDropDownMenu *)menu didSelectRowAtIndexPath:(DOPIndexPath *)indexPath {
    NSLog(@"column:%li row:%li", (long)indexPath.column, (long)indexPath.row);
    NSArray *Arr = _Categories[indexPath.column];
    
    if([Arr[indexPath.row] isKindOfClass:[GFCategoriesModel class]])
    {
        GFCategoriesModel *model = Arr[indexPath.row];
        [_parameters setObject:model.Id forKey:@"categoryId"];
    }
    else
    {
         NSString *model = Arr[indexPath.row];
    }
    self.page = 1;
    [self loadData:self.refreshView];
}
- (void)viewDidLoad {
    
    _parameters = [[NSMutableDictionary alloc]initWithDictionary:@{@"categoryId":@""}];
    [super viewDidLoad];
     _Categories = [NSMutableArray array];
    [self loadDataCategories];
    
    
    [FPTools setNavBar:self andTitle:self.title];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"LeftMenu"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] style:UIBarButtonItemStylePlain target:self action:@selector(presentMenu)];
    [FPTools setNavBar:self LeftBtnWithImageFile:@"LeftMenu" orTitle:nil andSEL:@selector(presentMenu) RightBtnWithImageFile:@"FPHome" orRightTitle:nil andSEL:@selector(presentRMenu)];

   
    
}


-(void)loadData:(MJRefreshBaseView *)refreshView
{
    [_parameters setObject:@(self.page) forKey:@"pageIndex"];
    [_parameters setObject:@(10) forKey:@"pageCount"];
    
    [FPApiRequst getRequstWithUrl:Activities_GetActivities andView:self andMSG:@"加载中..." andParameters:_parameters Block:^(id objc, NSError *error)
    {
        if (!error)
        {
            if (self.page==1)
            {
                [self.dateList removeAllObjects];
            }
            for (NSDictionary *item in objc[@"Data"]) {
                [self.dateList addObject:[[GFActivityModel alloc]initWithDic:item]];
            }
            [self doneWithView:refreshView];
        }
    }];
    [self doneWithView:refreshView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return self.dateList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GFMainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[GFMainCell alloc]init];
    }
    GFActivityModel *ActivityModel = self.dateList[indexPath.row];
    [cell.BackGroupImageView setImageWithURL:[NSURL URLWithString:ActivityModel.ActivityPhoto] placeholderImage:[UIImage imageNamed:@"image"]];
    cell.contentLabel.text = ActivityModel.ActivityName;
    cell.AddressLabel.text = ActivityModel.AreaTagName;
    [cell.tagList setTags:ActivityModel.tagList];
//    cell.dateTimeLabel.text = 
    // Configure the cell...
    
    return cell;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return _menu;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 231;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GFActivityDetailViewController *DetailC = [[GFActivityDetailViewController alloc]init];
    GFActivityModel *model = self.dateList[indexPath.row];
    DetailC.model = model;
    NSLog(@"model.Id == %@",model.Id);
    [self.navigationController pushViewController:DetailC animated:YES];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showRightMenu:(UIButton *)sender {
    NSMutableArray *styleItems = [NSMutableArray array];
    for (GFCategoriesModel *model in _Categories)
    {
        [styleItems addObject:[RWDropdownMenuItem itemWithText:model.TypeName image:nil action:^{
            [sender setTitle:model.TypeName forState:UIControlStateNormal];
            [_parameters setObject:model.Id forKey:@"categoryId"];
            [self loadData:self.refreshView];
        }]];
    }
    
    [RWDropdownMenu presentFromViewController:self withItems:styleItems align:RWDropdownMenuCellAlignmentCenter style:RWDropdownMenuStyleTranslucent navBarImage:nil completion:^{
        
    }];
    
}
@end
