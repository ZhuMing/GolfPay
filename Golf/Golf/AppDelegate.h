//
//  AppDelegate.h
//  Golf
//
//  Created by 朱明 on 15/1/28.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GFMainViewController.h"
#import "GFLeftMenuViewController.h"
#import "GFRightMenuViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong)GFLeftMenuViewController *leftMenuVC;
@property (nonatomic,strong)GFRightMenuViewController *RightMenuVC;

@end

