//
//  FPTools.m
//  FindPeople
//
//  Created by 朱明 on 14/11/12.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import "FPTools.h"
#import <UIKit/UIKit.h>
#import <ifaddrs.h>
#import <arpa/inet.h>
#include <netdb.h>
#include <net/if.h>
#include <dlfcn.h>
#import <CommonCrypto/CommonDigest.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>


@implementation FPTools


// 根据nib创建对象
id loadObjectFromNib(NSString *nib, Class cls, id owner) {
    NSArray *nibs = [[NSBundle mainBundle]  loadNibNamed:nib owner:owner options:nil];
    for (id oneObj in nibs) {
        if ([oneObj isKindOfClass:cls]) {
            return oneObj;
        }
    }
    return nil;
}
+ (BOOL)isMobileNumber:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+(void)setNavBar:(UIViewController*)controller
        andTitle:(NSString*)title
{
    [controller.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg.png"] forBarMetrics:UIBarMetricsCompact];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        controller.navigationItem.title = title;
        controller.edgesForExtendedLayout = UIRectEdgeNone;
        controller.navigationController.navigationBar.translucent=NO;
//        [controller.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:10.0/255.0 green:34.0/255.0 blue:66.0/255.0 alpha:1.0]];
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor darkTextColor],
                                    NSForegroundColorAttributeName, nil];
        [controller.navigationController.navigationBar setTitleTextAttributes:attributes];
    }
}

+ (void)setNavBar:(UIViewController *)controller LeftBtnWithImageFile:(NSString *)imgFile orTitle:(NSString *)title andSEL:(SEL)select
RightBtnWithImageFile:(NSString *)RightimgFile orRightTitle:(NSString *)Righttitle andSEL:(SEL)Rightselect
{
    //    NSLog(@"%@",title);
    //NSAssert 条件为YES 继续执行
    //    NSAssert((imgFile!=nil&&title==nil)||(imgFile==nil&&title!=nil), @"cteate left btn only can chose one type,with img or title,you must set one with nil");
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 40, 40);
    
    if(title!=nil && title.length>0)
        [btn setTitle:title forState:UIControlStateNormal];
    else
        [btn setImage:[UIImage imageNamed:imgFile] forState:UIControlStateNormal];
    
    [btn addTarget:controller action:select forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:btn];
    controller.navigationItem.leftBarButtonItem = leftBtn;
    
    
    if(Righttitle!=nil && Righttitle.length>0)
    {
        controller.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:Righttitle style:UIBarButtonItemStylePlain target:controller action:Rightselect];
        [controller.navigationItem.rightBarButtonItem setTintColor:[UIColor grayColor]];
        controller.navigationItem.rightBarButtonItem.tag =100;
    }
    else
    {
        UIButton *rbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rbtn.frame = CGRectMake(0, 0, 40, 40);
        [rbtn setImage:[UIImage imageNamed:RightimgFile] forState:UIControlStateNormal];
        
        [rbtn addTarget:controller action:Rightselect forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rBtn = [[UIBarButtonItem alloc] initWithCustomView:rbtn];
        
        controller.navigationItem.rightBarButtonItem = rBtn;
        controller.navigationItem.rightBarButtonItem.tag =100;
    }
}
//身份证号
+ (BOOL) validateIdentityCard: (NSString *)identityCard
{
    BOOL flag;
    if (identityCard.length <= 0) {
        flag = NO;
        return flag;
    }
    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    return [identityCardPredicate evaluateWithObject:identityCard];
}
@end
