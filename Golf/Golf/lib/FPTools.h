//
//  FPTools.h
//  FindPeople
//
//  Created by 朱明 on 14/11/12.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface FPTools : NSObject

id loadObjectFromNib(NSString *nib, Class cls, id owner);
+(void)setNavBar:(UIViewController*)controller
        andTitle:(NSString*)title;
+ (BOOL)isMobileNumber:(NSString *)mobileNum;
+ (void)setNavBar:(UIViewController *)controller LeftBtnWithImageFile:(NSString *)imgFile orTitle:(NSString *)title andSEL:(SEL)select
RightBtnWithImageFile:(NSString *)RightimgFile orRightTitle:(NSString *)Righttitle andSEL:(SEL)Rightselect;
+ (BOOL) validateIdentityCard: (NSString *)identityCard;
@end
