//
//  GFBassController.h
//  GolfFun
//
//  Created by Lee on 14-5-22.
//  Copyright (c) 2014年 ZHU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"

@interface GFBaseController : UITableViewController
@property(nonatomic,assign)NSInteger page;
@property(nonatomic,strong)NSMutableArray *dateList;
@property (nonatomic,strong)MJRefreshHeaderView *header;
@property (nonatomic,strong)MJRefreshFooterView *footer;
@property (nonatomic,strong)MJRefreshBaseView *refreshView;

@property (nonatomic,strong)NSObject *params;


- (id)initWithParams:(id)params;
- (void)doneWithView:(MJRefreshBaseView *)refreshView;
-(void)loadData:(MJRefreshBaseView *)refreshView;
@end
