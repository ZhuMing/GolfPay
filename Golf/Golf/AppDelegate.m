//
//  AppDelegate.m
//  Golf
//
//  Created by 朱明 on 15/1/28.
//  Copyright (c) 2015年 ZHU. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    UIAlertView *alv = [[UIAlertView alloc]initWithTitle:@"请输入IP" message:@"请输入IP" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
    alv.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alv show];
    
    
   
    return YES;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UITextField *nickNameText=[alertView textFieldAtIndex:0];
    [nickNameText resignFirstResponder];
    [[NSUserDefaults standardUserDefaults]setObject:nickNameText.text forKey:@"api"];
    _leftMenuVC = [[GFLeftMenuViewController alloc]init];
    _RightMenuVC = [[GFRightMenuViewController alloc]init];
    GFMainViewController *mainC = [[GFMainViewController alloc]init];
    UINavigationController *MainNav = [[UINavigationController alloc]initWithRootViewController:mainC];
    
    mainC.title = @"top 100";
    mainC.type = Top100;
    PPRevealSideViewController *revealSideViewController = [[PPRevealSideViewController alloc] initWithRootViewController:MainNav];
    // 预记载侧滑视图控制器的左侧视图控制器
    [revealSideViewController preloadViewController:_leftMenuVC forSide:PPRevealSideDirectionLeft withOffset:100];
    // 预记载侧滑视图控制器的右侧视图控制器
    [revealSideViewController preloadViewController:_RightMenuVC forSide:PPRevealSideDirectionRight withOffset:200];
    
    // 设置当视图关闭状态时支持的滑动手势方式
    [revealSideViewController setPanInteractionsWhenClosed:PPRevealSideInteractionNavigationBar|PPRevealSideInteractionContentView];
    self.window.rootViewController = revealSideViewController;
   
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
